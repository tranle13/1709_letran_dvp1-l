﻿namespace TranLe_CE03
{
    // Name: Tran Le
    // Class: Visual Framework
    // Term: 1712
    // Code Exercise Name: Custom Events
    // Number: 03
    partial class DataInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.File = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.List = new System.Windows.Forms.ToolStripMenuItem();
            this.Display = new System.Windows.Forms.ToolStripMenuItem();
            this.Clear = new System.Windows.Forms.ToolStripMenuItem();
            this.grb_PersonalInformation = new System.Windows.Forms.GroupBox();
            this.chk_MMO = new System.Windows.Forms.CheckBox();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_AddPlayer = new System.Windows.Forms.Button();
            this.cbo_MainPlayDevice = new System.Windows.Forms.ComboBox();
            this.lbl_MainPlayDevice = new System.Windows.Forms.Label();
            this.nud_PlayHoursPerDay = new System.Windows.Forms.NumericUpDown();
            this.lbl_PlayHoursPerDay = new System.Windows.Forms.Label();
            this.lbl_GameGenre = new System.Windows.Forms.Label();
            this.txt_FavoriteGame = new System.Windows.Forms.TextBox();
            this.lbl_FavoriteGame = new System.Windows.Forms.Label();
            this.txt_Username = new System.Windows.Forms.TextBox();
            this.lbl_Username = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.grb_PersonalInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_PlayHoursPerDay)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.File,
            this.List});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(566, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // File
            // 
            this.File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Exit});
            this.File.Name = "File";
            this.File.Size = new System.Drawing.Size(64, 36);
            this.File.Text = "File";
            // 
            // Exit
            // 
            this.Exit.CheckOnClick = true;
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(151, 38);
            this.Exit.Text = "Exit";
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // List
            // 
            this.List.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Display,
            this.Clear});
            this.List.Name = "List";
            this.List.Size = new System.Drawing.Size(62, 36);
            this.List.Text = "List";
            // 
            // Display
            // 
            this.Display.CheckOnClick = true;
            this.Display.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Display.Name = "Display";
            this.Display.Size = new System.Drawing.Size(191, 38);
            this.Display.Text = "Display";
            this.Display.Click += new System.EventHandler(this.Display_Click);
            // 
            // Clear
            // 
            this.Clear.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(191, 38);
            this.Clear.Text = "Clear";
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // grb_PersonalInformation
            // 
            this.grb_PersonalInformation.Controls.Add(this.chk_MMO);
            this.grb_PersonalInformation.Controls.Add(this.btn_Clear);
            this.grb_PersonalInformation.Controls.Add(this.btn_AddPlayer);
            this.grb_PersonalInformation.Controls.Add(this.cbo_MainPlayDevice);
            this.grb_PersonalInformation.Controls.Add(this.lbl_MainPlayDevice);
            this.grb_PersonalInformation.Controls.Add(this.nud_PlayHoursPerDay);
            this.grb_PersonalInformation.Controls.Add(this.lbl_PlayHoursPerDay);
            this.grb_PersonalInformation.Controls.Add(this.lbl_GameGenre);
            this.grb_PersonalInformation.Controls.Add(this.txt_FavoriteGame);
            this.grb_PersonalInformation.Controls.Add(this.lbl_FavoriteGame);
            this.grb_PersonalInformation.Controls.Add(this.txt_Username);
            this.grb_PersonalInformation.Controls.Add(this.lbl_Username);
            this.grb_PersonalInformation.Location = new System.Drawing.Point(12, 46);
            this.grb_PersonalInformation.Name = "grb_PersonalInformation";
            this.grb_PersonalInformation.Size = new System.Drawing.Size(542, 387);
            this.grb_PersonalInformation.TabIndex = 2;
            this.grb_PersonalInformation.TabStop = false;
            this.grb_PersonalInformation.Text = "Personal Information";
            // 
            // chk_MMO
            // 
            this.chk_MMO.AutoSize = true;
            this.chk_MMO.Location = new System.Drawing.Point(226, 135);
            this.chk_MMO.Name = "chk_MMO";
            this.chk_MMO.Size = new System.Drawing.Size(96, 29);
            this.chk_MMO.TabIndex = 2;
            this.chk_MMO.Text = "MMO";
            this.chk_MMO.UseVisualStyleBackColor = true;
            // 
            // btn_Clear
            // 
            this.btn_Clear.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_Clear.Location = new System.Drawing.Point(11, 321);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(190, 53);
            this.btn_Clear.TabIndex = 6;
            this.btn_Clear.Text = "Clear";
            this.btn_Clear.UseVisualStyleBackColor = false;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_AddPlayer
            // 
            this.btn_AddPlayer.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_AddPlayer.Location = new System.Drawing.Point(336, 321);
            this.btn_AddPlayer.Name = "btn_AddPlayer";
            this.btn_AddPlayer.Size = new System.Drawing.Size(190, 53);
            this.btn_AddPlayer.TabIndex = 5;
            this.btn_AddPlayer.Text = "Add Player";
            this.btn_AddPlayer.UseVisualStyleBackColor = false;
            this.btn_AddPlayer.Click += new System.EventHandler(this.btn_AddPlayer_Click);
            // 
            // cbo_MainPlayDevice
            // 
            this.cbo_MainPlayDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_MainPlayDevice.FormattingEnabled = true;
            this.cbo_MainPlayDevice.Items.AddRange(new object[] {
            "Phone",
            "Tablet",
            "Laptop",
            "Computer",
            "Console",
            "Other"});
            this.cbo_MainPlayDevice.Location = new System.Drawing.Point(226, 245);
            this.cbo_MainPlayDevice.Name = "cbo_MainPlayDevice";
            this.cbo_MainPlayDevice.Size = new System.Drawing.Size(220, 33);
            this.cbo_MainPlayDevice.TabIndex = 4;
            // 
            // lbl_MainPlayDevice
            // 
            this.lbl_MainPlayDevice.AutoSize = true;
            this.lbl_MainPlayDevice.Location = new System.Drawing.Point(6, 248);
            this.lbl_MainPlayDevice.Name = "lbl_MainPlayDevice";
            this.lbl_MainPlayDevice.Size = new System.Drawing.Size(185, 25);
            this.lbl_MainPlayDevice.TabIndex = 7;
            this.lbl_MainPlayDevice.Text = "Main Play Device:";
            // 
            // nud_PlayHoursPerDay
            // 
            this.nud_PlayHoursPerDay.Location = new System.Drawing.Point(326, 190);
            this.nud_PlayHoursPerDay.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.nud_PlayHoursPerDay.Name = "nud_PlayHoursPerDay";
            this.nud_PlayHoursPerDay.Size = new System.Drawing.Size(120, 31);
            this.nud_PlayHoursPerDay.TabIndex = 3;
            // 
            // lbl_PlayHoursPerDay
            // 
            this.lbl_PlayHoursPerDay.AutoSize = true;
            this.lbl_PlayHoursPerDay.Location = new System.Drawing.Point(6, 180);
            this.lbl_PlayHoursPerDay.Name = "lbl_PlayHoursPerDay";
            this.lbl_PlayHoursPerDay.Size = new System.Drawing.Size(280, 50);
            this.lbl_PlayHoursPerDay.TabIndex = 5;
            this.lbl_PlayHoursPerDay.Text = "How many hours per day do\r\nyou play this game?";
            // 
            // lbl_GameGenre
            // 
            this.lbl_GameGenre.AutoSize = true;
            this.lbl_GameGenre.Location = new System.Drawing.Point(6, 136);
            this.lbl_GameGenre.Name = "lbl_GameGenre";
            this.lbl_GameGenre.Size = new System.Drawing.Size(140, 25);
            this.lbl_GameGenre.TabIndex = 4;
            this.lbl_GameGenre.Text = "Game Genre:";
            // 
            // txt_FavoriteGame
            // 
            this.txt_FavoriteGame.Location = new System.Drawing.Point(226, 86);
            this.txt_FavoriteGame.Name = "txt_FavoriteGame";
            this.txt_FavoriteGame.Size = new System.Drawing.Size(300, 31);
            this.txt_FavoriteGame.TabIndex = 1;
            // 
            // lbl_FavoriteGame
            // 
            this.lbl_FavoriteGame.AutoSize = true;
            this.lbl_FavoriteGame.Location = new System.Drawing.Point(6, 89);
            this.lbl_FavoriteGame.Name = "lbl_FavoriteGame";
            this.lbl_FavoriteGame.Size = new System.Drawing.Size(159, 25);
            this.lbl_FavoriteGame.TabIndex = 2;
            this.lbl_FavoriteGame.Text = "Favorite Game:\r\n";
            // 
            // txt_Username
            // 
            this.txt_Username.Location = new System.Drawing.Point(226, 41);
            this.txt_Username.Name = "txt_Username";
            this.txt_Username.Size = new System.Drawing.Size(300, 31);
            this.txt_Username.TabIndex = 0;
            // 
            // lbl_Username
            // 
            this.lbl_Username.AutoSize = true;
            this.lbl_Username.Location = new System.Drawing.Point(6, 44);
            this.lbl_Username.Name = "lbl_Username";
            this.lbl_Username.Size = new System.Drawing.Size(116, 25);
            this.lbl_Username.TabIndex = 0;
            this.lbl_Username.Text = "Username:";
            // 
            // DataInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 445);
            this.Controls.Add(this.grb_PersonalInformation);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "DataInput";
            this.Text = "Data Input";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grb_PersonalInformation.ResumeLayout(false);
            this.grb_PersonalInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_PlayHoursPerDay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem File;
        private System.Windows.Forms.ToolStripMenuItem Exit;
        private System.Windows.Forms.ToolStripMenuItem List;
        private System.Windows.Forms.ToolStripMenuItem Display;
        private System.Windows.Forms.ToolStripMenuItem Clear;
        private System.Windows.Forms.GroupBox grb_PersonalInformation;
        private System.Windows.Forms.CheckBox chk_MMO;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Button btn_AddPlayer;
        private System.Windows.Forms.ComboBox cbo_MainPlayDevice;
        private System.Windows.Forms.Label lbl_MainPlayDevice;
        private System.Windows.Forms.NumericUpDown nud_PlayHoursPerDay;
        private System.Windows.Forms.Label lbl_PlayHoursPerDay;
        private System.Windows.Forms.Label lbl_GameGenre;
        private System.Windows.Forms.TextBox txt_FavoriteGame;
        private System.Windows.Forms.Label lbl_FavoriteGame;
        private System.Windows.Forms.TextBox txt_Username;
        private System.Windows.Forms.Label lbl_Username;
    }
}

