﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

    // Name: Tran Le
    // Class: Visual Framework
    // Term: 1712
    // Code Exercise Name: Custom Events
    // Number: 03
namespace TranLe_CE03
{
    public partial class DisplayInfo : Form
    {
        // Public event handler decalration
        public event EventHandler SelectToView;
        public event EventHandler ClearInMain;
        public event EventHandler CloseForm;

        // Property for accessing the info of Listbox in this class
        public int GetIndex
        {
            get
            {
                return lst_DataView.SelectedIndex;
            }
        }

        public DisplayInfo()
        {
            InitializeComponent();
        }

        // Method to populate the listbox when a player is added
        public void Addition(object sender, EventArgs e)
        {
            try
            {
                lst_DataView.Items.Clear();
                DataInput newAddition = (DataInput)sender;
                for (int i = 0; i < newAddition.listOfPlayers.Count; i++)
                {
                    lst_DataView.Items.Add(newAddition.listOfPlayers[i]);
                }
            }
            catch
            {
                MessageBox.Show("The list is empty. Try adding one or some players first.");
            }
        }
        
        // Method to clear the listbox
        public void ClearAll(object sender, EventArgs e)
        {
            lst_DataView.Items.Clear();
        }

        // Clear button in Toolstrip is used to clear Listbox and List
        private void Clear_Click(object sender, EventArgs e)
        {
            ClearInMain?.Invoke(this, new EventArgs());
            ClearAll(this, e);
        }

        // Code to show info on the main window for the selected item in second window
        private void lst_DataView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lst_DataView.Items.Count > 0)
            {
                SelectToView?.Invoke(this, new EventArgs());
            }
        }

        // Raise event handler method
        private void DisplayInfo_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseForm?.Invoke(this, new EventArgs());
        }
    }
}
