﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

    // Name: Tran Le
    // Class: Visual Framework
    // Term: 1712
    // Code Exercise Name: Custom Events
    // Number: 03
namespace TranLe_CE03
{
    public partial class DataInput : Form
    {
        // Property to access and change the values of variables
        public DataInfoType InputValues
        {
            get
            {
                DataInfoType player = new DataInfoType();
                player.username = txt_Username.Text;
                player.favGame = txt_FavoriteGame.Text;
                player.isMMO = chk_MMO.Checked;
                player.PlayHoursPerDay = nud_PlayHoursPerDay.Value;
                player.playDevice = cbo_MainPlayDevice.SelectedIndex;
                return player;
            }
            set
            {
                txt_Username.Text = value.username;
                txt_FavoriteGame.Text = value.favGame;
                chk_MMO.Checked = value.isMMO;
                nud_PlayHoursPerDay.Value = value.PlayHoursPerDay;
                cbo_MainPlayDevice.SelectedIndex = value.playDevice;
            }
        }

        // Create a List to store info of players
        public List<DataInfoType> listOfPlayers = new List<DataInfoType>();

        // Public event handler declaration
        public event EventHandler ClearInDisplay;
        public event EventHandler Addition;

        public DataInput()
        {
            InitializeComponent();
        }

        // Exit application when user chooses Exit
        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Clear button at the bottom is used to clear the user input controls
        private void btn_Clear_Click(object sender, EventArgs e)
        {
            txt_Username.Clear();
            txt_FavoriteGame.Clear();
            chk_MMO.Checked = false;
            nud_PlayHoursPerDay.Value = 0m;
            cbo_MainPlayDevice.SelectedIndex = -1;
        }

        // Code to execute when display is clicked
        private void Display_Click(object sender, EventArgs e)
        {
            DisplayInfo newWindow = new DisplayInfo();
            Display.Checked = true;
            // Set the newWindow's event to be handled
            Addition += newWindow.Addition;
            Addition?.Invoke(this, new EventArgs());
            newWindow.CloseForm += NewWindow_Close;
            newWindow.SelectToView += NewWindow_SelectToView;
            newWindow.ClearInMain += NewWindow_ClearAllInfo;
            ClearInDisplay += newWindow.ClearAll;
            ClearInDisplay += NewWindow_ClearAllInfo;
            newWindow.Show();
        }

        // Event handler method to clear everything in 1st form
        private void NewWindow_ClearAllInfo(object sender, EventArgs e)
        {
            listOfPlayers.Clear();
            btn_Clear_Click(this, e);
        }

        // Event handler method to check or uncheck the display option when 2nd window is running or not
        private void NewWindow_Close(object sender, EventArgs e)
        {
            Display.Checked = false;
        }

        // Event handler method to display the selected item in listbox on 2nd form
        private void NewWindow_SelectToView(object sender, EventArgs e)
        {
            if (listOfPlayers.Count > 0)
            {
                DisplayInfo selectToView = (DisplayInfo)sender;
                int index = selectToView.GetIndex;
                txt_Username.Text = listOfPlayers[index].username;
                txt_FavoriteGame.Text = listOfPlayers[index].favGame;
                chk_MMO.Checked = listOfPlayers[index].isMMO;
                nud_PlayHoursPerDay.Value = listOfPlayers[index].PlayHoursPerDay;
                cbo_MainPlayDevice.SelectedIndex = listOfPlayers[index].playDevice;
            }
        }

        // Code to invoke event when user clicks the Add Player button
        private void btn_AddPlayer_Click(object sender, EventArgs e)
        {
            listOfPlayers.Add(InputValues);
            if (Display.Checked == true)
            {
                Addition?.Invoke(this, new EventArgs());
            }
            btn_Clear_Click(this, e);
        }

        // Code to clear the List and Listbox when user clicks Clear in the Menu Strip
        private void Clear_Click(object sender, EventArgs e)
        {
            if (Display.Checked == true)
            {
                ClearInDisplay?.Invoke(this, new EventArgs());
            }
            ClearInDisplay?.Invoke(this, new EventArgs());
        }
    }
}
