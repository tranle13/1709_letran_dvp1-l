﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

    // Name: Tran Le
    // Class: Visual Framework
    // Term: 1712
    // Code Exercise Name: Custom Events
    // Number: 03
namespace TranLe_CE03
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DataInput());
        }
    }
}
