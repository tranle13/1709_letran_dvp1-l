﻿namespace TranLe_CE03
{
    // Name: Tran Le
    // Class: Visual Framework
    // Term: 1712
    // Code Exercise Name: Custom Events
    // Number: 03
    partial class DisplayInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DisplayInfo));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.Clear = new System.Windows.Forms.ToolStripButton();
            this.lst_DataView = new System.Windows.Forms.ListBox();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Clear});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(646, 39);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // Clear
            // 
            this.Clear.Image = ((System.Drawing.Image)(resources.GetObject("Clear.Image")));
            this.Clear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(105, 36);
            this.Clear.Text = "Clear";
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // lst_DataView
            // 
            this.lst_DataView.FormattingEnabled = true;
            this.lst_DataView.ItemHeight = 25;
            this.lst_DataView.Location = new System.Drawing.Point(12, 86);
            this.lst_DataView.Name = "lst_DataView";
            this.lst_DataView.Size = new System.Drawing.Size(622, 779);
            this.lst_DataView.TabIndex = 1;
            this.lst_DataView.SelectedIndexChanged += new System.EventHandler(this.lst_DataView_SelectedIndexChanged);
            // 
            // DisplayInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(646, 875);
            this.Controls.Add(this.lst_DataView);
            this.Controls.Add(this.toolStrip1);
            this.Name = "DisplayInfo";
            this.Text = "Display Information";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DisplayInfo_FormClosed);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton Clear;
        private System.Windows.Forms.ListBox lst_DataView;
    }
}