﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    // Name: Tran Le
    // Class: Visual Framework
    // Term: 1712
    // Code Exercise Name: Custom Events
    // Number: 03
namespace TranLe_CE03
{
    public class DataInfoType
    {
        public string username;
        public string favGame;
        public bool isMMO;
        public decimal PlayHoursPerDay;
        public int playDevice;

        public override string ToString()
        {
            return $"- Name: {username}; Favorite Game: {favGame}";
        }
    }
}
