﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TranLe_CE02
{
    public partial class PlayersCount : Form
    {
        public PlayersCount()
        {
            InitializeComponent();
        }

        // Make a property to set the values to the stats
        public void StatsDisplay(string value_1, string value_2)
        {
            txt_NumMMOPlayers.Text = value_1;
            txt_NumOtherPlayers.Text = value_2;
        }
    }
}
