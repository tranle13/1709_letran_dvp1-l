﻿namespace TranLe_CE02
{
    partial class PlayersCount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_NumMMOPlayers = new System.Windows.Forms.Label();
            this.lbl_NumOtherPlayers = new System.Windows.Forms.Label();
            this.txt_NumOtherPlayers = new System.Windows.Forms.TextBox();
            this.txt_NumMMOPlayers = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_NumMMOPlayers
            // 
            this.lbl_NumMMOPlayers.AutoSize = true;
            this.lbl_NumMMOPlayers.Location = new System.Drawing.Point(76, 54);
            this.lbl_NumMMOPlayers.Name = "lbl_NumMMOPlayers";
            this.lbl_NumMMOPlayers.Size = new System.Drawing.Size(253, 25);
            this.lbl_NumMMOPlayers.TabIndex = 0;
            this.lbl_NumMMOPlayers.Text = "Number of MMO Players:";
            // 
            // lbl_NumOtherPlayers
            // 
            this.lbl_NumOtherPlayers.AutoSize = true;
            this.lbl_NumOtherPlayers.Location = new System.Drawing.Point(76, 118);
            this.lbl_NumOtherPlayers.Name = "lbl_NumOtherPlayers";
            this.lbl_NumOtherPlayers.Size = new System.Drawing.Size(265, 25);
            this.lbl_NumOtherPlayers.TabIndex = 1;
            this.lbl_NumOtherPlayers.Text = "Numbers of Other Players:";
            // 
            // txt_NumOtherPlayers
            // 
            this.txt_NumOtherPlayers.Location = new System.Drawing.Point(364, 115);
            this.txt_NumOtherPlayers.Name = "txt_NumOtherPlayers";
            this.txt_NumOtherPlayers.ReadOnly = true;
            this.txt_NumOtherPlayers.Size = new System.Drawing.Size(100, 31);
            this.txt_NumOtherPlayers.TabIndex = 2;
            // 
            // txt_NumMMOPlayers
            // 
            this.txt_NumMMOPlayers.Location = new System.Drawing.Point(364, 51);
            this.txt_NumMMOPlayers.Name = "txt_NumMMOPlayers";
            this.txt_NumMMOPlayers.ReadOnly = true;
            this.txt_NumMMOPlayers.Size = new System.Drawing.Size(100, 31);
            this.txt_NumMMOPlayers.TabIndex = 3;
            // 
            // PlayersCount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 199);
            this.Controls.Add(this.txt_NumMMOPlayers);
            this.Controls.Add(this.txt_NumOtherPlayers);
            this.Controls.Add(this.lbl_NumOtherPlayers);
            this.Controls.Add(this.lbl_NumMMOPlayers);
            this.Name = "PlayersCount";
            this.Text = "Players Counter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_NumMMOPlayers;
        private System.Windows.Forms.Label lbl_NumOtherPlayers;
        private System.Windows.Forms.TextBox txt_NumOtherPlayers;
        private System.Windows.Forms.TextBox txt_NumMMOPlayers;
    }
}