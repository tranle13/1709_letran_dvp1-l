﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TranLe_CE02
{
    // Name: Tran Le
    // Class: Visual Framework
    // Term: 1712
    // Code Exercise Name: Passing Data and Multiple Forms
    // Number: 02
    public partial class Form1 : Form
    {
        // Property to build new objects
        Player dataSource
        {
            get
            {
                Player currentPlayer = new Player();
                currentPlayer.username = txt_Username.Text;
                currentPlayer.favoriteGame = txt_FavoriteGame.Text;
                currentPlayer.isMMO = chk_MMO.Checked;
                currentPlayer.playHours = nud_PlayHoursPerDay.Value;
                currentPlayer.playDevice = cbo_MainPlayDevice.SelectedIndex;
                return currentPlayer;
            }
        }

        // Variables to store the number of players on each list
        int numMMO = 0;
        int numOther = 0;

        public Form1()
        {
            InitializeComponent();
        }

        // Close the application
        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Add user input to appropriate list box and clear the user input controls
        private void btn_AddPlayer_Click(object sender, EventArgs e)
        {
            if (cbo_MainPlayDevice.SelectedItem != null)
            {
                if (chk_MMO.Checked == true)
                {
                    lst_MMOPlayers.Items.Add(dataSource);
                }
                else
                {
                    lst_OtherPlayers.Items.Add(dataSource);
                }
            }
            txt_Username.Clear();
            txt_FavoriteGame.Clear();
            chk_MMO.Checked = false;
            nud_PlayHoursPerDay.Value = 0m;
            cbo_MainPlayDevice.SelectedIndex = -1;
        }

        // Repopulate user input controls with the info of the MMO Player
        private void lst_MMOPlayers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lst_MMOPlayers.SelectedItem != null)
            {
                lst_OtherPlayers.SelectedIndex = -1;
                txt_Username.Text = ((Player)lst_MMOPlayers.SelectedItem).username;
                txt_FavoriteGame.Text = ((Player)lst_MMOPlayers.SelectedItem).favoriteGame;
                chk_MMO.Checked = ((Player)lst_MMOPlayers.SelectedItem).isMMO;
                nud_PlayHoursPerDay.Value = ((Player)lst_MMOPlayers.SelectedItem).playHours;
                cbo_MainPlayDevice.SelectedIndex = ((Player)lst_MMOPlayers.SelectedItem).playDevice;
            }
        }

        // Repopulate user input controls with the info of the Other Player
        private void lst_OtherPlayers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lst_OtherPlayers.SelectedItem != null)
            {
                txt_Username.Clear();
                txt_FavoriteGame.Clear();
                chk_MMO.Checked = false;
                nud_PlayHoursPerDay.Value = 0m;
                cbo_MainPlayDevice.SelectedIndex = -1;
                lst_MMOPlayers.SelectedIndex = -1;
                txt_Username.Text = ((Player)lst_OtherPlayers.SelectedItem).username;
                txt_FavoriteGame.Text = ((Player)lst_OtherPlayers.SelectedItem).favoriteGame;
                chk_MMO.Checked = ((Player)lst_OtherPlayers.SelectedItem).isMMO;
                nud_PlayHoursPerDay.Value = ((Player)lst_OtherPlayers.SelectedItem).playHours;
                cbo_MainPlayDevice.SelectedIndex = ((Player)lst_OtherPlayers.SelectedItem).playDevice;
            }
        }

        // Display the stats for both categories of players
        private void Display_Click(object sender, EventArgs e)
        {
            PlayersCount statsCounter = new PlayersCount();
            statsCounter.StatsDisplay(lst_MMOPlayers.Items.Count.ToString(), lst_OtherPlayers.Items.Count.ToString());
            statsCounter.ShowDialog();
        }

        // Button to delete the selected item
        private void btn_DeletePlayer_Click(object sender, EventArgs e)
        {
            if (lst_MMOPlayers.SelectedItem != null)
            {
                lst_MMOPlayers.Items.RemoveAt(lst_MMOPlayers.SelectedIndex);
            }
            else if (lst_OtherPlayers.SelectedItem != null)
            {
                lst_OtherPlayers.Items.RemoveAt(lst_OtherPlayers.SelectedIndex);
            }
            txt_Username.Clear();
            txt_FavoriteGame.Clear();
            chk_MMO.Checked = false;
            nud_PlayHoursPerDay.Value = 0m;
            cbo_MainPlayDevice.SelectedIndex = -1;
        }

        // Button to change the selected player in MMO Players list to Other Players list
        private void btn_MMOToOtherPlayer_Click(object sender, EventArgs e)
        {
            if (lst_MMOPlayers.Items.Count != 0 && lst_MMOPlayers.SelectedItem != null)
            {
                lst_OtherPlayers.Items.Add(lst_MMOPlayers.SelectedItem);
                ((Player)lst_MMOPlayers.SelectedItem).isMMO = false;
                lst_MMOPlayers.Items.RemoveAt(lst_MMOPlayers.SelectedIndex);
                chk_MMO.Checked = false;
            }
        }

        // Button to change the selected player in Other Players list to MMO Players list
        private void btn_OtherToMMOPlayer_Click(object sender, EventArgs e)
        {
            if (lst_OtherPlayers.Items.Count != 0 && lst_OtherPlayers.SelectedItem != null)
            {
                lst_MMOPlayers.Items.Add(lst_OtherPlayers.SelectedItem);
                ((Player)lst_OtherPlayers.SelectedItem).isMMO = true;
                lst_OtherPlayers.Items.RemoveAt(lst_OtherPlayers.SelectedIndex);
                chk_MMO.Checked = true;
            }
        }

        // Code for saving file by writing into file using StreamWriter
        private void Save_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.FileName = "MMO Players Data";
            save.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            if (save.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter writer = new StreamWriter(save.FileName))
                {
                    foreach (Player item in lst_MMOPlayers.Items)
                    {
                        writer.WriteLine($"MMO {item.username} {item.favoriteGame} {item.isMMO} {item.playHours} {item.playDevice}");
                    }
                    foreach (Player gamer in lst_OtherPlayers.Items)
                    {
                        writer.WriteLine($"Other {gamer.username} {gamer.favoriteGame} {gamer.isMMO} {gamer.playHours} {gamer.playDevice}");
                    }
                }
            }
            numMMO = lst_MMOPlayers.Items.Count;
            numOther = lst_OtherPlayers.Items.Count;
        }
        
        // Button to clear all input controls for user
        private void btn_Clear_Click(object sender, EventArgs e)
        {
            txt_Username.Clear();
            txt_FavoriteGame.Clear();
            chk_MMO.Checked = false;
            nud_PlayHoursPerDay.Value = 0m;
            cbo_MainPlayDevice.SelectedIndex = -1;
        }

        // Code to load info into the lists for user
        private void Load_Click(object sender, EventArgs e)
        {
            OpenFileDialog load = new OpenFileDialog();
            load.Filter = "txt files (*.txt)|*.txt";
            load.Title = "Select a file with .txt extension";

            if (load.ShowDialog() == DialogResult.OK)
            {
                lst_MMOPlayers.Items.Clear();
                lst_OtherPlayers.Items.Clear();
                List<string> playerInfo = new List<string>();
                using (StreamReader reader = new StreamReader(load.FileName))
                {
                    try
                    {
                        while (reader.EndOfStream == false)
                        {
                            string info = reader.ReadLine();
                            if (info.Contains("MMO") == true)
                            {
                                string[] separateInfo = info.Split(' ');
                                txt_Username.Text = separateInfo[1];
                                txt_FavoriteGame.Text = separateInfo[2];
                                if (separateInfo[3] == "true")
                                {
                                    chk_MMO.Checked = true;
                                }
                                else
                                {
                                    chk_MMO.Checked = false;
                                }
                                nud_PlayHoursPerDay.Value = Convert.ToDecimal(separateInfo[4]);
                                cbo_MainPlayDevice.SelectedIndex = Convert.ToInt32(separateInfo[5]);
                                lst_MMOPlayers.Items.Add(dataSource);
                            }
                            else if (info.Contains("Other") == true)
                            {
                                string[] separateInfo = info.Split(' ');
                                txt_Username.Text = separateInfo[1];
                                txt_FavoriteGame.Text = separateInfo[2];
                                if (separateInfo[3] == "true")
                                {
                                    chk_MMO.Checked = true;
                                }
                                else
                                {
                                    chk_MMO.Checked = false;
                                }
                                nud_PlayHoursPerDay.Value = Convert.ToDecimal(separateInfo[4]);
                                cbo_MainPlayDevice.SelectedIndex = Convert.ToInt32(separateInfo[5]);
                                lst_OtherPlayers.Items.Add(dataSource);
                            }
                        }
                    }
                    catch (IOException exception)
                    {
                        MessageBox.Show(exception.Message);
                    }
                }
            }

        }
    }
}
