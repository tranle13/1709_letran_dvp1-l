﻿namespace TranLe_CE02
{
    // Name: Tran Le
    // Class: Visual Framework
    // Term: 1712
    // Code Exercise Name: Passing Data and Multiple Forms
    // Number: 02
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.File = new System.Windows.Forms.ToolStripMenuItem();
            this.Load = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.Save = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.statsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Display = new System.Windows.Forms.ToolStripMenuItem();
            this.grb_PersonalInformation = new System.Windows.Forms.GroupBox();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_AddPlayer = new System.Windows.Forms.Button();
            this.cbo_MainPlayDevice = new System.Windows.Forms.ComboBox();
            this.lbl_MainPlayDevice = new System.Windows.Forms.Label();
            this.nud_PlayHoursPerDay = new System.Windows.Forms.NumericUpDown();
            this.lbl_PlayHoursPerDay = new System.Windows.Forms.Label();
            this.lbl_GameGenre = new System.Windows.Forms.Label();
            this.txt_FavoriteGame = new System.Windows.Forms.TextBox();
            this.lbl_FavoriteGame = new System.Windows.Forms.Label();
            this.txt_Username = new System.Windows.Forms.TextBox();
            this.lbl_Username = new System.Windows.Forms.Label();
            this.grb_MMOPlayers = new System.Windows.Forms.GroupBox();
            this.lst_MMOPlayers = new System.Windows.Forms.ListBox();
            this.grb_OtherPlayers = new System.Windows.Forms.GroupBox();
            this.lst_OtherPlayers = new System.Windows.Forms.ListBox();
            this.btn_MMOToOtherPlayer = new System.Windows.Forms.Button();
            this.btn_OtherToMMOPlayer = new System.Windows.Forms.Button();
            this.btn_DeletePlayer = new System.Windows.Forms.Button();
            this.chk_MMO = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.grb_PersonalInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_PlayHoursPerDay)).BeginInit();
            this.grb_MMOPlayers.SuspendLayout();
            this.grb_OtherPlayers.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.File,
            this.statsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1474, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // File
            // 
            this.File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Load,
            this.toolStripMenuItem1,
            this.Save,
            this.toolStripMenuItem2,
            this.Exit});
            this.File.Name = "File";
            this.File.Size = new System.Drawing.Size(64, 36);
            this.File.Text = "File";
            // 
            // Load
            // 
            this.Load.Name = "Load";
            this.Load.Size = new System.Drawing.Size(268, 38);
            this.Load.Text = "Load";
            this.Load.Click += new System.EventHandler(this.Load_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(265, 6);
            // 
            // Save
            // 
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(268, 38);
            this.Save.Text = "Save";
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(265, 6);
            // 
            // Exit
            // 
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(268, 38);
            this.Exit.Text = "Exit";
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // statsToolStripMenuItem
            // 
            this.statsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Display});
            this.statsToolStripMenuItem.Name = "statsToolStripMenuItem";
            this.statsToolStripMenuItem.Size = new System.Drawing.Size(77, 36);
            this.statsToolStripMenuItem.Text = "Stats";
            // 
            // Display
            // 
            this.Display.Name = "Display";
            this.Display.Size = new System.Drawing.Size(268, 38);
            this.Display.Text = "Display";
            this.Display.Click += new System.EventHandler(this.Display_Click);
            // 
            // grb_PersonalInformation
            // 
            this.grb_PersonalInformation.Controls.Add(this.chk_MMO);
            this.grb_PersonalInformation.Controls.Add(this.btn_Clear);
            this.grb_PersonalInformation.Controls.Add(this.btn_AddPlayer);
            this.grb_PersonalInformation.Controls.Add(this.cbo_MainPlayDevice);
            this.grb_PersonalInformation.Controls.Add(this.lbl_MainPlayDevice);
            this.grb_PersonalInformation.Controls.Add(this.nud_PlayHoursPerDay);
            this.grb_PersonalInformation.Controls.Add(this.lbl_PlayHoursPerDay);
            this.grb_PersonalInformation.Controls.Add(this.lbl_GameGenre);
            this.grb_PersonalInformation.Controls.Add(this.txt_FavoriteGame);
            this.grb_PersonalInformation.Controls.Add(this.lbl_FavoriteGame);
            this.grb_PersonalInformation.Controls.Add(this.txt_Username);
            this.grb_PersonalInformation.Controls.Add(this.lbl_Username);
            this.grb_PersonalInformation.Location = new System.Drawing.Point(12, 54);
            this.grb_PersonalInformation.Name = "grb_PersonalInformation";
            this.grb_PersonalInformation.Size = new System.Drawing.Size(460, 415);
            this.grb_PersonalInformation.TabIndex = 1;
            this.grb_PersonalInformation.TabStop = false;
            this.grb_PersonalInformation.Text = "Personal Information";
            // 
            // btn_Clear
            // 
            this.btn_Clear.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_Clear.Location = new System.Drawing.Point(11, 351);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(190, 53);
            this.btn_Clear.TabIndex = 9;
            this.btn_Clear.Text = "Clear";
            this.btn_Clear.UseVisualStyleBackColor = false;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_AddPlayer
            // 
            this.btn_AddPlayer.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_AddPlayer.Location = new System.Drawing.Point(256, 351);
            this.btn_AddPlayer.Name = "btn_AddPlayer";
            this.btn_AddPlayer.Size = new System.Drawing.Size(190, 53);
            this.btn_AddPlayer.TabIndex = 5;
            this.btn_AddPlayer.Text = "Add Player";
            this.btn_AddPlayer.UseVisualStyleBackColor = false;
            this.btn_AddPlayer.Click += new System.EventHandler(this.btn_AddPlayer_Click);
            // 
            // cbo_MainPlayDevice
            // 
            this.cbo_MainPlayDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_MainPlayDevice.FormattingEnabled = true;
            this.cbo_MainPlayDevice.Items.AddRange(new object[] {
            "Phone",
            "Tablet",
            "Laptop",
            "Computer",
            "Console",
            "Other"});
            this.cbo_MainPlayDevice.Location = new System.Drawing.Point(226, 263);
            this.cbo_MainPlayDevice.Name = "cbo_MainPlayDevice";
            this.cbo_MainPlayDevice.Size = new System.Drawing.Size(220, 33);
            this.cbo_MainPlayDevice.TabIndex = 4;
            // 
            // lbl_MainPlayDevice
            // 
            this.lbl_MainPlayDevice.AutoSize = true;
            this.lbl_MainPlayDevice.Location = new System.Drawing.Point(6, 266);
            this.lbl_MainPlayDevice.Name = "lbl_MainPlayDevice";
            this.lbl_MainPlayDevice.Size = new System.Drawing.Size(185, 25);
            this.lbl_MainPlayDevice.TabIndex = 7;
            this.lbl_MainPlayDevice.Text = "Main Play Device:";
            // 
            // nud_PlayHoursPerDay
            // 
            this.nud_PlayHoursPerDay.Location = new System.Drawing.Point(326, 208);
            this.nud_PlayHoursPerDay.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.nud_PlayHoursPerDay.Name = "nud_PlayHoursPerDay";
            this.nud_PlayHoursPerDay.Size = new System.Drawing.Size(120, 31);
            this.nud_PlayHoursPerDay.TabIndex = 3;
            // 
            // lbl_PlayHoursPerDay
            // 
            this.lbl_PlayHoursPerDay.AutoSize = true;
            this.lbl_PlayHoursPerDay.Location = new System.Drawing.Point(6, 198);
            this.lbl_PlayHoursPerDay.Name = "lbl_PlayHoursPerDay";
            this.lbl_PlayHoursPerDay.Size = new System.Drawing.Size(280, 50);
            this.lbl_PlayHoursPerDay.TabIndex = 5;
            this.lbl_PlayHoursPerDay.Text = "How many hours per day do\r\nyou play this game?";
            // 
            // lbl_GameGenre
            // 
            this.lbl_GameGenre.AutoSize = true;
            this.lbl_GameGenre.Location = new System.Drawing.Point(6, 154);
            this.lbl_GameGenre.Name = "lbl_GameGenre";
            this.lbl_GameGenre.Size = new System.Drawing.Size(140, 25);
            this.lbl_GameGenre.TabIndex = 4;
            this.lbl_GameGenre.Text = "Game Genre:";
            // 
            // txt_FavoriteGame
            // 
            this.txt_FavoriteGame.Location = new System.Drawing.Point(226, 86);
            this.txt_FavoriteGame.Name = "txt_FavoriteGame";
            this.txt_FavoriteGame.Size = new System.Drawing.Size(220, 31);
            this.txt_FavoriteGame.TabIndex = 1;
            // 
            // lbl_FavoriteGame
            // 
            this.lbl_FavoriteGame.AutoSize = true;
            this.lbl_FavoriteGame.Location = new System.Drawing.Point(6, 89);
            this.lbl_FavoriteGame.Name = "lbl_FavoriteGame";
            this.lbl_FavoriteGame.Size = new System.Drawing.Size(159, 50);
            this.lbl_FavoriteGame.TabIndex = 2;
            this.lbl_FavoriteGame.Text = "Favorite Game:\r\n(no space)";
            // 
            // txt_Username
            // 
            this.txt_Username.Location = new System.Drawing.Point(226, 41);
            this.txt_Username.Name = "txt_Username";
            this.txt_Username.Size = new System.Drawing.Size(220, 31);
            this.txt_Username.TabIndex = 0;
            // 
            // lbl_Username
            // 
            this.lbl_Username.AutoSize = true;
            this.lbl_Username.Location = new System.Drawing.Point(6, 44);
            this.lbl_Username.Name = "lbl_Username";
            this.lbl_Username.Size = new System.Drawing.Size(116, 25);
            this.lbl_Username.TabIndex = 0;
            this.lbl_Username.Text = "Username:";
            // 
            // grb_MMOPlayers
            // 
            this.grb_MMOPlayers.Controls.Add(this.lst_MMOPlayers);
            this.grb_MMOPlayers.Location = new System.Drawing.Point(493, 54);
            this.grb_MMOPlayers.Name = "grb_MMOPlayers";
            this.grb_MMOPlayers.Size = new System.Drawing.Size(408, 863);
            this.grb_MMOPlayers.TabIndex = 2;
            this.grb_MMOPlayers.TabStop = false;
            this.grb_MMOPlayers.Text = "MMO Players";
            // 
            // lst_MMOPlayers
            // 
            this.lst_MMOPlayers.FormattingEnabled = true;
            this.lst_MMOPlayers.ItemHeight = 25;
            this.lst_MMOPlayers.Location = new System.Drawing.Point(16, 31);
            this.lst_MMOPlayers.Name = "lst_MMOPlayers";
            this.lst_MMOPlayers.Size = new System.Drawing.Size(378, 804);
            this.lst_MMOPlayers.TabIndex = 6;
            this.lst_MMOPlayers.SelectedIndexChanged += new System.EventHandler(this.lst_MMOPlayers_SelectedIndexChanged);
            // 
            // grb_OtherPlayers
            // 
            this.grb_OtherPlayers.Controls.Add(this.lst_OtherPlayers);
            this.grb_OtherPlayers.Location = new System.Drawing.Point(1054, 54);
            this.grb_OtherPlayers.Name = "grb_OtherPlayers";
            this.grb_OtherPlayers.Size = new System.Drawing.Size(408, 863);
            this.grb_OtherPlayers.TabIndex = 3;
            this.grb_OtherPlayers.TabStop = false;
            this.grb_OtherPlayers.Text = "Other Players";
            // 
            // lst_OtherPlayers
            // 
            this.lst_OtherPlayers.FormattingEnabled = true;
            this.lst_OtherPlayers.ItemHeight = 25;
            this.lst_OtherPlayers.Location = new System.Drawing.Point(15, 29);
            this.lst_OtherPlayers.Name = "lst_OtherPlayers";
            this.lst_OtherPlayers.Size = new System.Drawing.Size(378, 804);
            this.lst_OtherPlayers.TabIndex = 7;
            this.lst_OtherPlayers.SelectedIndexChanged += new System.EventHandler(this.lst_OtherPlayers_SelectedIndexChanged);
            // 
            // btn_MMOToOtherPlayer
            // 
            this.btn_MMOToOtherPlayer.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_MMOToOtherPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MMOToOtherPlayer.Location = new System.Drawing.Point(930, 282);
            this.btn_MMOToOtherPlayer.Name = "btn_MMOToOtherPlayer";
            this.btn_MMOToOtherPlayer.Size = new System.Drawing.Size(100, 80);
            this.btn_MMOToOtherPlayer.TabIndex = 6;
            this.btn_MMOToOtherPlayer.Text = ">";
            this.btn_MMOToOtherPlayer.UseVisualStyleBackColor = false;
            this.btn_MMOToOtherPlayer.Click += new System.EventHandler(this.btn_MMOToOtherPlayer_Click);
            // 
            // btn_OtherToMMOPlayer
            // 
            this.btn_OtherToMMOPlayer.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_OtherToMMOPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_OtherToMMOPlayer.Location = new System.Drawing.Point(930, 414);
            this.btn_OtherToMMOPlayer.Name = "btn_OtherToMMOPlayer";
            this.btn_OtherToMMOPlayer.Size = new System.Drawing.Size(100, 80);
            this.btn_OtherToMMOPlayer.TabIndex = 7;
            this.btn_OtherToMMOPlayer.Text = "<";
            this.btn_OtherToMMOPlayer.UseVisualStyleBackColor = false;
            this.btn_OtherToMMOPlayer.Click += new System.EventHandler(this.btn_OtherToMMOPlayer_Click);
            // 
            // btn_DeletePlayer
            // 
            this.btn_DeletePlayer.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_DeletePlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DeletePlayer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_DeletePlayer.Location = new System.Drawing.Point(930, 546);
            this.btn_DeletePlayer.Name = "btn_DeletePlayer";
            this.btn_DeletePlayer.Size = new System.Drawing.Size(100, 80);
            this.btn_DeletePlayer.TabIndex = 8;
            this.btn_DeletePlayer.Text = "x";
            this.btn_DeletePlayer.UseVisualStyleBackColor = false;
            this.btn_DeletePlayer.Click += new System.EventHandler(this.btn_DeletePlayer_Click);
            // 
            // chk_MMO
            // 
            this.chk_MMO.AutoSize = true;
            this.chk_MMO.Location = new System.Drawing.Point(226, 153);
            this.chk_MMO.Name = "chk_MMO";
            this.chk_MMO.Size = new System.Drawing.Size(96, 29);
            this.chk_MMO.TabIndex = 10;
            this.chk_MMO.Text = "MMO";
            this.chk_MMO.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1474, 929);
            this.Controls.Add(this.btn_DeletePlayer);
            this.Controls.Add(this.btn_OtherToMMOPlayer);
            this.Controls.Add(this.btn_MMOToOtherPlayer);
            this.Controls.Add(this.grb_OtherPlayers);
            this.Controls.Add(this.grb_MMOPlayers);
            this.Controls.Add(this.grb_PersonalInformation);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Personal Game Data";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grb_PersonalInformation.ResumeLayout(false);
            this.grb_PersonalInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_PlayHoursPerDay)).EndInit();
            this.grb_MMOPlayers.ResumeLayout(false);
            this.grb_OtherPlayers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem File;
        private System.Windows.Forms.ToolStripMenuItem Load;
        private System.Windows.Forms.ToolStripMenuItem Save;
        private System.Windows.Forms.ToolStripMenuItem Exit;
        private System.Windows.Forms.ToolStripMenuItem statsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Display;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.GroupBox grb_PersonalInformation;
        private System.Windows.Forms.ComboBox cbo_MainPlayDevice;
        private System.Windows.Forms.Label lbl_MainPlayDevice;
        private System.Windows.Forms.NumericUpDown nud_PlayHoursPerDay;
        private System.Windows.Forms.Label lbl_PlayHoursPerDay;
        private System.Windows.Forms.Label lbl_GameGenre;
        private System.Windows.Forms.TextBox txt_FavoriteGame;
        private System.Windows.Forms.Label lbl_FavoriteGame;
        private System.Windows.Forms.TextBox txt_Username;
        private System.Windows.Forms.Label lbl_Username;
        private System.Windows.Forms.Button btn_AddPlayer;
        private System.Windows.Forms.GroupBox grb_MMOPlayers;
        private System.Windows.Forms.GroupBox grb_OtherPlayers;
        private System.Windows.Forms.Button btn_MMOToOtherPlayer;
        private System.Windows.Forms.Button btn_OtherToMMOPlayer;
        private System.Windows.Forms.Button btn_DeletePlayer;
        private System.Windows.Forms.ListBox lst_MMOPlayers;
        private System.Windows.Forms.ListBox lst_OtherPlayers;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.CheckBox chk_MMO;
    }
}

