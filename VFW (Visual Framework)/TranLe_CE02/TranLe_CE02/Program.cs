﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TranLe_CE02
{
    // Name: Tran Le
    // Class: Visual Framework
    // Term: 1712
    // Code Exercise Name: Passing Data and Multiple Forms
    // NUmber: 02
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
