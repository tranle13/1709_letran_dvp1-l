﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE02
{
    class Player
    {
        public string username;
        public string favoriteGame;
        public bool isMMO;
        public decimal playHours;
        public int playDevice;

        public override string ToString()
        {
            return $"- Name: {username} - Game: {favoriteGame}";
        }
    }
}
