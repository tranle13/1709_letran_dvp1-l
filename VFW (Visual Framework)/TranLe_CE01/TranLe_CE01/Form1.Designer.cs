﻿namespace TranLe_CE01
{
    partial class Introduction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Load = new System.Windows.Forms.ToolStripMenuItem();
            this.Save = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.PersonalInformation = new System.Windows.Forms.GroupBox();
            this.rdo_No = new System.Windows.Forms.RadioButton();
            this.rdo_Yes = new System.Windows.Forms.RadioButton();
            this.lbl_HowManyDogs = new System.Windows.Forms.Label();
            this.nud_HowManyDogs = new System.Windows.Forms.NumericUpDown();
            this.lbl_DoYouHaveDog = new System.Windows.Forms.Label();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.txt_LastName = new System.Windows.Forms.TextBox();
            this.lbl_LastName = new System.Windows.Forms.Label();
            this.txt_FirstName = new System.Windows.Forms.TextBox();
            this.lbl_FirstName = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.PersonalInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_HowManyDogs)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(594, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Load,
            this.Save,
            this.Exit});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // Load
            // 
            this.Load.Name = "Load";
            this.Load.Size = new System.Drawing.Size(165, 38);
            this.Load.Text = "Load";
            this.Load.Click += new System.EventHandler(this.Load_Click);
            // 
            // Save
            // 
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(165, 38);
            this.Save.Text = "Save";
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Exit
            // 
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(165, 38);
            this.Exit.Text = "Exit";
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // PersonalInformation
            // 
            this.PersonalInformation.Controls.Add(this.rdo_No);
            this.PersonalInformation.Controls.Add(this.rdo_Yes);
            this.PersonalInformation.Controls.Add(this.lbl_HowManyDogs);
            this.PersonalInformation.Controls.Add(this.nud_HowManyDogs);
            this.PersonalInformation.Controls.Add(this.lbl_DoYouHaveDog);
            this.PersonalInformation.Controls.Add(this.btn_Clear);
            this.PersonalInformation.Controls.Add(this.txt_LastName);
            this.PersonalInformation.Controls.Add(this.lbl_LastName);
            this.PersonalInformation.Controls.Add(this.txt_FirstName);
            this.PersonalInformation.Controls.Add(this.lbl_FirstName);
            this.PersonalInformation.Location = new System.Drawing.Point(12, 52);
            this.PersonalInformation.Name = "PersonalInformation";
            this.PersonalInformation.Size = new System.Drawing.Size(570, 355);
            this.PersonalInformation.TabIndex = 1;
            this.PersonalInformation.TabStop = false;
            this.PersonalInformation.Text = "Personal Information";
            // 
            // rdo_No
            // 
            this.rdo_No.AutoSize = true;
            this.rdo_No.Location = new System.Drawing.Point(448, 162);
            this.rdo_No.Name = "rdo_No";
            this.rdo_No.Size = new System.Drawing.Size(70, 29);
            this.rdo_No.TabIndex = 11;
            this.rdo_No.TabStop = true;
            this.rdo_No.Text = "No";
            this.rdo_No.UseVisualStyleBackColor = true;
            // 
            // rdo_Yes
            // 
            this.rdo_Yes.AutoSize = true;
            this.rdo_Yes.Location = new System.Drawing.Point(328, 164);
            this.rdo_Yes.Name = "rdo_Yes";
            this.rdo_Yes.Size = new System.Drawing.Size(81, 29);
            this.rdo_Yes.TabIndex = 10;
            this.rdo_Yes.Text = "Yes";
            this.rdo_Yes.UseVisualStyleBackColor = true;
            // 
            // lbl_HowManyDogs
            // 
            this.lbl_HowManyDogs.AutoSize = true;
            this.lbl_HowManyDogs.Location = new System.Drawing.Point(40, 218);
            this.lbl_HowManyDogs.Name = "lbl_HowManyDogs";
            this.lbl_HowManyDogs.Size = new System.Drawing.Size(301, 25);
            this.lbl_HowManyDogs.TabIndex = 9;
            this.lbl_HowManyDogs.Text = "How many dogs do you have?";
            // 
            // nud_HowManyDogs
            // 
            this.nud_HowManyDogs.Location = new System.Drawing.Point(378, 216);
            this.nud_HowManyDogs.Name = "nud_HowManyDogs";
            this.nud_HowManyDogs.Size = new System.Drawing.Size(120, 31);
            this.nud_HowManyDogs.TabIndex = 8;
            // 
            // lbl_DoYouHaveDog
            // 
            this.lbl_DoYouHaveDog.AutoSize = true;
            this.lbl_DoYouHaveDog.Location = new System.Drawing.Point(40, 166);
            this.lbl_DoYouHaveDog.Name = "lbl_DoYouHaveDog";
            this.lbl_DoYouHaveDog.Size = new System.Drawing.Size(239, 25);
            this.lbl_DoYouHaveDog.TabIndex = 5;
            this.lbl_DoYouHaveDog.Text = "Do you have any dogs?";
            // 
            // btn_Clear
            // 
            this.btn_Clear.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_Clear.Location = new System.Drawing.Point(378, 285);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(175, 50);
            this.btn_Clear.TabIndex = 4;
            this.btn_Clear.Text = "Clear";
            this.btn_Clear.UseVisualStyleBackColor = false;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // txt_LastName
            // 
            this.txt_LastName.Location = new System.Drawing.Point(187, 100);
            this.txt_LastName.Name = "txt_LastName";
            this.txt_LastName.Size = new System.Drawing.Size(210, 31);
            this.txt_LastName.TabIndex = 3;
            // 
            // lbl_LastName
            // 
            this.lbl_LastName.AutoSize = true;
            this.lbl_LastName.Location = new System.Drawing.Point(40, 103);
            this.lbl_LastName.Name = "lbl_LastName";
            this.lbl_LastName.Size = new System.Drawing.Size(121, 25);
            this.lbl_LastName.TabIndex = 2;
            this.lbl_LastName.Text = "Last Name:";
            // 
            // txt_FirstName
            // 
            this.txt_FirstName.Location = new System.Drawing.Point(187, 50);
            this.txt_FirstName.Name = "txt_FirstName";
            this.txt_FirstName.Size = new System.Drawing.Size(210, 31);
            this.txt_FirstName.TabIndex = 1;
            // 
            // lbl_FirstName
            // 
            this.lbl_FirstName.AutoSize = true;
            this.lbl_FirstName.Location = new System.Drawing.Point(39, 53);
            this.lbl_FirstName.Name = "lbl_FirstName";
            this.lbl_FirstName.Size = new System.Drawing.Size(122, 25);
            this.lbl_FirstName.TabIndex = 0;
            this.lbl_FirstName.Text = "First Name:";
            // 
            // Introduction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 419);
            this.Controls.Add(this.PersonalInformation);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Introduction";
            this.Text = "Introduction";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.PersonalInformation.ResumeLayout(false);
            this.PersonalInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_HowManyDogs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Load;
        private System.Windows.Forms.ToolStripMenuItem Save;
        private System.Windows.Forms.ToolStripMenuItem Exit;
        private System.Windows.Forms.GroupBox PersonalInformation;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.TextBox txt_LastName;
        private System.Windows.Forms.Label lbl_LastName;
        private System.Windows.Forms.TextBox txt_FirstName;
        private System.Windows.Forms.Label lbl_FirstName;
        private System.Windows.Forms.Label lbl_HowManyDogs;
        private System.Windows.Forms.NumericUpDown nud_HowManyDogs;
        private System.Windows.Forms.Label lbl_DoYouHaveDog;
        private System.Windows.Forms.RadioButton rdo_No;
        private System.Windows.Forms.RadioButton rdo_Yes;
    }
}

