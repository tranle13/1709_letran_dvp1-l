﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TranLe_CE01
{
    // Name: Tran Le
    // Class: Visual Framework
    // Term: 1712
    // Code exercise name: Introduction to Windows Forms
    // Number: 01
    public partial class Introduction : Form
    {
        public Introduction()
        {
            InitializeComponent();
        }

        // Code for exit button to close window when user clicks it
        private void Exit_Click(object sender, EventArgs e)
        {
            MessageBox.Show("See you later!");
            Application.Exit();
        }

        // Code for saving file by writing into file using StreamWriter
        private void Save_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.FileName = "Personal Information.txt";
            save.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            if (save.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter writer = new StreamWriter(save.FileName))
                {
                    // Check whether the textbox is null or not
                    if (txt_FirstName.Text != null)
                    {
                        writer.Write($"{txt_FirstName.Text} ");
                    }
                    else
                    {
                        writer.Write(" ");
                    }
                    // Check whether the textbox is null or not
                    if (txt_LastName.Text != null)
                    {
                        writer.Write($"{txt_LastName.Text} ");
                    }
                    else
                    {
                        writer.Write(" ");
                    }
                    // Check whether the radio button is null or not
                    if (rdo_Yes.Checked == true)
                    {
                        writer.Write($"{rdo_Yes.Text} ");
                    }
                    else if (rdo_No.Checked == true)
                    {
                        writer.Write($"{rdo_No.Text} ");
                    }
                    else
                    {
                        writer.Write(" ");
                    }
                    writer.Write($"{nud_HowManyDogs.Value}");
                }
            }
        }

        // Code for opening file by reading from the file user choose using StreamReader
        private void Load_Click(object sender, EventArgs e)
        {
            OpenFileDialog load = new OpenFileDialog();
            load.Filter = "txt files (*.txt)|*.txt";
            load.Title = "Select a file with .txt extension";

            if (load.ShowDialog() == DialogResult.OK)
            {
                using (StreamReader reader = new StreamReader(load.FileName))
                {
                    try
                    {
                        while (reader.EndOfStream == false)
                        {
                            string info = reader.ReadLine();
                            string[] userInfo = info.Split(' ');
                            txt_FirstName.Text = userInfo[0];
                            txt_LastName.Text = userInfo[1];
                            if (userInfo[2] == "Yes")
                            {
                                rdo_Yes.Checked = true;
                            }
                            else if (userInfo[2] == "No")
                            {
                                rdo_No.Checked = true;
                            }
                            nud_HowManyDogs.Value = decimal.Parse(userInfo[3]);
                        }
                    }
                    catch (IOException exception)
                    {
                        MessageBox.Show(exception.Message);
                    }
                }
            }
        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            txt_FirstName.Text = "";
            txt_LastName.Text = "";
            rdo_Yes.Checked = false;
            rdo_No.Checked = false;
            nud_HowManyDogs.Value = 0;
        }
    }
}
