#REPOSITORY GENERAL OVERVIEW #
---

Hi, my name is Sunny, I'm from Vietnam and I'm studying at Full Sail University. I'm currently working on programming using C# and Visual Studio.

This repository is made not only to preserve all of my product but also help me gain an understanding of how Version Control works, which is a significantly useful tool for developers and programmers nowadays. As the number of code exercises increase, I will have a wide variety of works to put in my resume. It includes:

1. **Codes for challenges in Portfolio 1**: 
	* Swap Info
	* Backwards
	* Age Convert
	* Temp Convert
	* Menu

2. **Codes exercises of ASD**:
    * Fundamentals Review
	* Classes
	* Inheritance
	* Interfaces and abstract classes
	* Debugging
	* Generics and Collections
	
3. **Code exercises of Portfolio 2**:
	* Sorting and Q&A
	* Show Grades and Color Dictionary
	* R.P.S and Card Counting
	* Mad Libs & Smiley Face
	* Final code of Project 2

4. **Code exercises of Portfolio 3**:
	* GroceryList or PartyList