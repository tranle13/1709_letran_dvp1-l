﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE06
{
    class Program
    {
        static void Main(string[] args)
        {
            CollectionManager currentManager = null;
            //string[] keys = currentManager.MultipleCollections.Keys.ToArray();
            bool condition = true;
            Validation validate = new Validation();
            while (condition)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(" ******** COLLECTIONS MANAGER ******** \n");
                Console.ResetColor();
                Console.Write("       1. Create collection manager\n" +
                              "       2. Create a card\n" +
                              "       3. Add a card to a collection\n" +
                              "       4. Remove a card from a collection\n" +
                              "       5. Display a collection\n" +
                              "       6. Display all collection\n" +
                              "       7. Exit\n" +
                              " Chooose: ");
                Console.ForegroundColor = ConsoleColor.Cyan;
                string choice = Console.ReadLine().ToLower();
                Console.ResetColor();
                Console.Clear();
                switch (choice)
                {
                    case "1":
                    case "create collection manager":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(" ******** CREATE COLLECTION MANAGER ******** \n");
                            Console.ResetColor();
                            currentManager = new CollectionManager();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine(" Successfully created a collection manager!");
                            Console.ResetColor();
                        }
                        break;
                    case "2":
                    case "create a card":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(" ******** CREATE A CARD ******** \n");
                            Console.ResetColor();
                            if (currentManager != null)
                            {
                                Console.Write(" What will you name your card?\n - ");
                                string cardName = validate.LettersOnly();
                                Console.Write(" What would be its description?\n - ");
                                string cardDesc = validate.LettersOnly();
                                Console.Write(" What is card's value?\n - ");
                                decimal cardValue = validate.NumbersOnly();

                                Card newCard = new Card(cardName, cardDesc, cardValue);
                                currentManager.AvailableCards.Add(newCard);
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine(" Successfully create a card!");
                                Console.ResetColor();

                            }
                            else
                            {
                                Console.WriteLine(" (>'-')> Please create a collection manager first.");
                            }
                        }
                        break;
                    case "3":
                    case "add a card to a collection":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(" ******** ADD A CARD TO COLLECTION ******** \n");
                            Console.ResetColor();
                            if (currentManager != null && currentManager.AvailableCards.Count != 0)
                            {
                                currentManager.AddCard();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine(" Successfully add a card to your collection!");
                                Console.ResetColor();
                            }
                            else
                            {
                                Console.WriteLine(" (>'-')> Please create a collection manager and cards first.");
                            }
                        }
                        break;
                    case "4":
                    case "remove a card from a collection":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(" ******** REMOVE A CARD FROM A COLLECTION ******** \n");
                            Console.ResetColor();
                            if (currentManager != null)
                            {
                                currentManager.RemoveCard();
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine(" Successfully removed a card from your collection!");
                                Console.ResetColor();
                            }
                            else
                            {
                                Console.WriteLine(" (>'-')> Please create a collection manager and a collection first.");
                            }
                        }
                        break;
                    case "5":
                    case "display a collection":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(" ******** DISPLAY A COLLECTION ******** \n");
                            Console.ResetColor();
                            if (currentManager != null && currentManager.MultipleCollections != null)
                            {
                                currentManager.DisplayACollection();
                            }
                            else
                            {
                                Console.WriteLine(" (>'-')> Uh oh, I don't have any collections. Try making one first.");
                            }
                        }
                        break;
                    case "6":
                    case "display all collections":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(" ******** DISPLAY ALL COLLECTIONS ******** \n");
                            Console.ResetColor();
                            if (currentManager != null && currentManager.MultipleCollections != null)
                            {
                                currentManager.DisplayAllCollections();
                            }
                            else
                            {
                                Console.WriteLine(" (>'-')> Uh oh, I don't have any collections. Try making one first.");
                            }
                        }
                        break;
                    case "7":
                    case "exit":
                        {
                            condition = false;
                        }
                        break;
                    default:
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("\n (>'-')> I don't offer this option. Sorry! Try again!");
                            Console.ResetColor();
                        }
                        break;
                }
                Console.WriteLine("\n Press any key to continue.");
                Console.ReadKey();
            }
        }
    }
}
