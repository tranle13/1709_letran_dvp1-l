﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE06
{
    class CollectionManager
    {
        List<Card> _availableCards;
        Dictionary<string, List<Card>> _multipleCollections;
        Validation validate = new Validation();

        public CollectionManager()
        {
            _availableCards = new List<Card>();
            _multipleCollections = new Dictionary<string, List<Card>>();
        }
        public List<Card> AvailableCards
        {
            get
            {
                return _availableCards;
            }
        }

        public Dictionary<string, List<Card>> MultipleCollections
        {
            get
            {
                return _multipleCollections;
            }
        }

        private int SelectCollection(string message, string[] keys)
        {
            for (int i = 0; i < MultipleCollections.Count; i++)
            {
                Console.WriteLine($" {i + 1}. {keys[i]}");
            }

            Console.Write(message);
            int selection = validate.ChoiceInRange(1, keys.Length);
            --selection;

            return selection;
        }

        public void DisplayAllCollections()
        {
            string[] keys = MultipleCollections.Keys.ToArray();
            if (keys.Length > 0)
            {
                for (int i = 0; i < MultipleCollections.Count; i++)
                {
                    Console.WriteLine("------------------------------------------");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($" {i + 1}. {keys[i]}");
                    Console.ResetColor();
                    if (MultipleCollections[keys[i]].Count == 0)
                    {
                        Console.WriteLine("   Empty.");
                    }
                    for (int j = 0; j < MultipleCollections[keys[i]].Count; j++)
                    {
                            Console.WriteLine($" {j + 1}. {MultipleCollections[keys[i]][j]}");
                    }
                    Console.WriteLine("------------------------------------------");
                }
            }
            else
            {
                Console.WriteLine(" (>'-')> I'm sorry. You have to create a collection to display.");
            }
        }

        public void AddCard()
        {
            string[] keys = MultipleCollections.Keys.ToArray();
            if (keys.Length > 0)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(" List of available collections:");
                Console.ResetColor();
                for (int i = 0; i < MultipleCollections.Count; i++)
                {
                    Console.WriteLine($" {i + 1}. {keys[i]}");
                }
            }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\n List of available cards:");
            Console.ResetColor();
            for (int i = 0; i < AvailableCards.Count; i++)
            {
                Console.WriteLine($" { i + 1 }. {AvailableCards[i]}\n");
            }
            Console.Write(" What's the name of the collection that you want to add card to?\n - ");
            string selection = validate.LettersOnly();
            if (MultipleCollections.ContainsKey(selection) == false)
            {
                MultipleCollections.Add(selection, new List<Card>());
            }
            Console.Write(" Which card do you want to add? (number only)\n - ");
            int cardNum = validate.ChoiceInRange(1, AvailableCards.Count);
            --cardNum;
            MultipleCollections[selection].Add(AvailableCards[cardNum]);
            AvailableCards.RemoveAt(cardNum);
        }

        public void RemoveCard()
        {
            string[] keys = MultipleCollections.Keys.ToArray();
            if (keys.Length > 0)
            {
                int selection = SelectCollection(" Which collection that you want to remove card from? (number only)\n - ", keys);
                if (MultipleCollections[keys[selection]].Count > 0)
                {
                    for (int j = 0; j < MultipleCollections[keys[selection]].Count; j++)
                    {
                        Console.WriteLine($"\n {j + 1}. {MultipleCollections[keys[selection]][j]}");
                    }

                    Console.Write(" Which card do you want to remove from this collection? (number only)\n - ");
                    int removeNum = validate.ChoiceInRange(1, MultipleCollections[keys[selection]].Count);
                    --removeNum;
                    AvailableCards.Add(MultipleCollections[keys[selection]][removeNum]);
                    MultipleCollections[keys[selection]].RemoveAt(removeNum);
                }
                else
                {
                    Console.WriteLine(" (>'-')> This collection doesn't have any cards yet. Add some cards then come back.");
                }
            }
            else
            {
                Console.WriteLine(" (>*^*)> I have no collections. Make one first then come back.");
            }
        }

        public void DisplayACollection()
        {
            string[] keys = MultipleCollections.Keys.ToArray();
            if (keys.Length > 0)
            {
                int chosenNumber = SelectCollection(" Which collection would you like to see? (number only)\n - ", keys);
                if (MultipleCollections[keys[chosenNumber]].Count > 0)
                {
                    for (int j = 0; j < MultipleCollections[keys[chosenNumber]].Count; j++)
                    {
                        Console.WriteLine($" {j + 1}. {MultipleCollections[keys[chosenNumber]][j]}");
                    }
                }
                else
                {
                    Console.WriteLine(" This collection doesn't have any card yet. Try adding one first");
                }
            }
            else
            {
                Console.WriteLine(" (>'-')> I'm sorry. You have to create a collection to display.");
            }

        }
    }
}
