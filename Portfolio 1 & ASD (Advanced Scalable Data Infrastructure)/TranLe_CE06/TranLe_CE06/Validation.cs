﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TranLe_CE06
{
    class Validation
    {
        public string LettersOnly()
        {
            string input = Console.ReadLine();
            while (!Regex.IsMatch(input, "^[A-Za-z ]+$"))
            {
                Console.Write("\n ( *^*) Letters only, please try again!\r\n - ");
                input = Console.ReadLine();
            }

            return input;

        }

        public int NumbersOnly()
        {
            int newNum;
            string number = Console.ReadLine();
            while (!int.TryParse(number, out newNum))
            {
                Console.Write("\n (>'-')> Man, I ask for numbers, not that!\n - ");
                number = Console.ReadLine();
            }


            return newNum;
        }

        public int ChoiceInRange(int min, int max)
        {
            int newNum;
            string number = Console.ReadLine();
            while(!int.TryParse(number, out newNum) || (int.Parse(number) < min || int.Parse(number) > max))
            {
                Console.Write("\n (>'-')> That input isn't on the list so please try again!\n - ");
                number = Console.ReadLine();
            }

            return newNum;
        }




    }
}
