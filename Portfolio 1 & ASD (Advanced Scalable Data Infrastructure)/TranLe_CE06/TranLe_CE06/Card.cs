﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE06
{
    class Card
    {
        private string _cardName;
        private string _cardDesc;
        private decimal _cardValue;

        public Card(string cardName, string cardDesc, decimal cardValue)
        {
            _cardName = cardName;
            _cardDesc = cardDesc;
            _cardValue = cardValue;
        }

        public string CardName
        {
            get
            {
                return _cardName;
            }
        }

        public string CardDesc
        {
            get
            {
                return _cardDesc;
            }
        }

        public decimal CardValue
        {
            get
            {
                return _cardValue;
            }
        }

        public override string ToString()
        {
            return $"Card name: {_cardName}\n    Card description: {_cardDesc}\n    Card value: {_cardValue}";
        }
    }
}
