﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE01
{
    class Program
    {
        static void Main(string[] args)
        {
            MandF call = new MandF();
            string[] tank = { "burgundy", "turquoise", "yellow", "lavender", "lime", "peach", "turquoise", "peach", "burgundy", "lavender",
                              "yellow", "burgundy", "yellow", "turquoise", "peach", "turquoise" };
            float[] length = { 10.8f, 12f, 13.8f, 8.3f, 2.4f, 1.2f, 3.6f, 7.4f, 6.1f, 10.2f, 11.3f, 4.5f, 23.1f, 15.4f, 16.3f, 9.2f };
            string[] color = { "burgundy", "turquoise", "yellow", "lime", "peach", "lavender" };
            Console.WriteLine("\n****** You want the biggest or smallest fish? (type \"b\" or \"s\")");
            Console.Write("       Answer: ");
            string size = call.Validate();
            Console.WriteLine("\n****** Pick the color that you like (numbers only)!");
            call.List(color);
            Console.Write("       Search: ");
            int choice = call.Validation();

            float value = call.Value(tank, color, length, choice);
            float minValue = 0f;
            if (size == "b")
            {
                float maxValue = call.Value(tank, color, length, choice);
                Console.WriteLine($"\n  -->  The biggest {color[choice - 1]} fish is {maxValue} centimeters.");
            }
            else
            {
                for (int i = 0; i < tank.Length; i++)
                {
                    if (color[choice - 1] == tank[i])
                    {
                        if (value > length[i])
                        {
                            minValue = length[i];
                            value = length[i];
                        }
                    }
                }
                Console.WriteLine($"\n  -->  The smallest {color[choice - 1]} fish is {minValue} centimeters.");
            }
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
