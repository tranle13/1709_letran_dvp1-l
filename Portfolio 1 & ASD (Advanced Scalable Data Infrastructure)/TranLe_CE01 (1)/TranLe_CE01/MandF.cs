﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE01
{
    class MandF
    {
        public int Validation()
        {
            int choice;
            string pick = Console.ReadLine();
            while (!int.TryParse(pick, out choice) || !(0 < int.Parse(pick) && int.Parse(pick) < 7))
            {
                Console.WriteLine("\n   ->  Uh oh D: Please try again!");
                Console.Write("       Search: ");
                pick = Console.ReadLine();
            }
            return choice;
        }

        public string Validate()
        {
            string size = Console.ReadLine();
            while (size != "b" && size != "s")
            {
                Console.WriteLine("\n   ->  Nah! Please try again!");
                Console.Write("       Answer: ");
                size = Console.ReadLine();
            }
            return size;
        }

        public void List(string[] colors)
        {
            int counter = 1;
            foreach (string hue in colors)
            {
                Console.WriteLine($"            {counter}. {hue}");
                counter++;
            }
        }

        public float Value(string[] tank, string[] color, float[] length, int choice)
        {
            float maxValue = 0f;
            for (int h = 0; h < tank.Length; h++)
            {
                if (color[choice - 1] == tank[h])
                {
                    if (maxValue < length[h])
                    {
                        maxValue = length[h];
                    }
                }
            }
            return maxValue;
        }
    }
}
