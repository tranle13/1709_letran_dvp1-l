﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE03
{
    class Course
    {
        private string _courseTitle;
        private string _courseDescription;
        private Teacher _teacherInfo;
        private Student[] _studentInfo;

        // Constructor

        public Course(string courseTitle, string courseDescription, int numberOfStudents)
        {
            _courseTitle = courseTitle;
            _courseDescription = courseDescription;
            _studentInfo = new Student[numberOfStudents];
        }

        public string CourseTitle
        {
            get
            {
                return _courseTitle;
            }
        }

        public string CourseDescription
        {
            get
            {
                return _courseDescription;
            }
        }

        public Teacher TeacherInfo
        {
            get
            {
                return _teacherInfo;
            }
            set
            {
                _teacherInfo = value;
            }
        }

        public int NumberOfStudent
        {
            get
            {
                return _studentInfo.Length;
            }
        }

        public Student[] StudentInfo
        {
            get
            {
                return _studentInfo;
            }
            set
            {
                _studentInfo = value;
            }
        }
    }
}
