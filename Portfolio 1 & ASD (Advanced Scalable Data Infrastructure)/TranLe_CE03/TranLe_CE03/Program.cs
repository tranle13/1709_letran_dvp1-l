﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TranLe_CE03
{
    class Program
    {
        static void Main(string[] args)
        {
            Course currentCourse = null;
            Validation check = new Validation();
            while (true)
            {
                Console.Write(" ********** COURSE TRACKER ********** \n" +
                                  "         1. Create course\n" +
                                  "         2. Create teacher\n" +
                                  "         3. Add students\n" +
                                  "         4. Display information\n" +
                                  "         5. Change student grade\n" + 
                                  "         6. Exit\n" +
                                  "    Choose: ");
                string choice = check.Choose();
                string choiceAsString = check.ChoiceAsString(choice);
                int choiceAsNumber = check.ChoiceAsNumber(choice);
                Console.Clear();

                if (choiceAsNumber == 1 || choiceAsString == "create course")
                {
                    Console.WriteLine(" ******** CREATE COURSE ******** ");
                    Console.Write(" What's the name of the course? (letters only)\n - ");
                    string courseName = check.LetterAndSpace();
                    Console.Write(" What's the description for this course?\n - ");
                    string courseDescription = check.LetterAndSpace();
                    Console.Write(" How many students would be attend in this course?\n - ");
                    int numberOfStudents = check.NumberOfStudents();
                    currentCourse = new Course(courseName, courseDescription, numberOfStudents);
                }

                else if (choiceAsNumber == 2 || choiceAsString == "create teacher")
                {
                    Console.WriteLine(" ******** CREATE TEACHER ******** ");
                    if (currentCourse != null)
                    {
                        Console.Write(" What's the teacher's name?\n - ");
                        string teacherName = check.LetterAndSpace();
                        Console.Write(" How old is the teacher?\n - ");
                        int teacherAge = check.AgeCheck();
                        Console.Write(" What subjects does he/she teach? (separate by comma, no blank space)\n - ");
                        string subjects = check.SubjectsOfTeacher();
                        string[] separateSubjects = subjects.Split(',');
                        currentCourse.TeacherInfo = new Teacher(teacherName, subjects, teacherAge, separateSubjects);
                    }
                    else if (currentCourse == null)
                    {
                        Console.WriteLine(" (>*-*)> You haven't created any courses yet.\n         Please go back to\"Create Course\" to make a new one.");
                    }
                }

                else if (choiceAsNumber == 3 || choiceAsString == "add students")
                {
                    Console.WriteLine(" ******** ADD STUDENTS ******** ");
                    if (currentCourse != null)
                    {
                        Console.WriteLine($"\n You can only create{currentCourse.NumberOfStudent} student(s).");

                        for (int j = 0; j < currentCourse.NumberOfStudent; j++)
                        {
                            Console.Write($" {j + 1}. Student name: ");
                            string studentName = check.LetterAndSpace();
                            Console.Write("   Student description (separate by comma, no blank space):\n   ");
                            string studentDescription = Console.ReadLine();
                            Console.Write("   Student age: ");
                            int studentAge = check.AgeCheck();
                            Console.Write("   Student grade: ");
                            decimal studentGrade = check.Grade();
                            currentCourse.StudentInfo[j] = new Student(studentName, studentDescription, studentAge, studentGrade);
                        }
                    }

                    else if (currentCourse == null)
                    {
                        Console.WriteLine(" (>*-*)> You haven't created any course yet.\n         Please go back to\"Create Course\" to make a new one.");
                    }
                }

                else if (choiceAsNumber == 4 || choiceAsString == "display information")
                {
                    Console.WriteLine(" ******** DISPLAY INFORMATION ******** ");
                    if (currentCourse != null && currentCourse.TeacherInfo != null && currentCourse.StudentInfo[0] != null)
                    {
                        // Course info
                        Console.WriteLine("\n     ** Course Information **");
                        Console.WriteLine($" - Course name: {currentCourse.CourseTitle}.");
                        Console.WriteLine($" - Course description: {currentCourse.CourseDescription}.");

                        // Teacher info
                        Console.WriteLine("\n     ** Teacher Information **");
                        Console.WriteLine($" - Teacher: {currentCourse.TeacherInfo.PersonName}.");
                        Console.WriteLine($" - Teacher's knowledge: {string.Join(", ", currentCourse.TeacherInfo.TeacherKnowledge)}");


                        // Student info
                        Console.WriteLine("\n     ** Student Information **");
                        Console.WriteLine($" - There're {currentCourse.NumberOfStudent} student(s) in this course.");
                        for (int h = 0; h < currentCourse.NumberOfStudent; h++)
                        {
                            Console.WriteLine($" {h + 1}. Student: {currentCourse.StudentInfo[h].PersonName}");
                            Console.WriteLine($"    Description: {currentCourse.StudentInfo[h].PersonDescription}");
                            Console.WriteLine($"    Age: {currentCourse.StudentInfo[h].PersonAge}");
                            Console.WriteLine($"    Grade: {currentCourse.StudentInfo[h].StudentGrade}\n");
                        }
                    }
                    else if (currentCourse == null || currentCourse.TeacherInfo == null || currentCourse.StudentInfo[0] == null)
                    {
                        Console.WriteLine(" (>*-*)> I have nothing to display.\n         Please go back to\"Create Course\", \"Create teacher\", \"Add students\" to create information.");
                    }
                }
                else if (choiceAsNumber == 5 || choiceAsString == "change student grade")
                {

                }
                else
                {
                    break;
                }
                Console.WriteLine("\n Press any key to go back...");
                Console.ReadKey();
                Console.Clear();

            }
        }
    }
}
