﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE03
{
    class Student : Person
    {
        private decimal _studentGrade;

        // Constructor
        public Student(string personName, string personDescription, int personAge, decimal studentGrade) : base(personName, personDescription, personAge)
        {
            _studentGrade = studentGrade;
        }

        public decimal StudentGrade
        {
            get
            {
                return _studentGrade;
            }
            set
            {
                _studentGrade = value;
            }
        }
    }
}
