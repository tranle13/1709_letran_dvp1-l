﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TranLe_CE03
{
    class Validation
    {
        //public void Choice(string choice)
        //{
        //    string toLowerCase = choice.ToLower();
        //    int pick = int.Parse(Console.ReadLine());
        //    while(!int.TryParse(choice, out pick) && !(0 < int.Parse(choice) && int.Parse(choice) < 7) || choice.ToLower() != "create course" || choice.ToLower() != "create teacher" || choice.ToLower() != "add student"
        //                 || choice.ToLower() != "display information" || choice.ToLower() != "change student grade" || choice.ToLower() != "exit")
        //    {
        //        Console.Write("\n   (>'-')> Invalid! Please try again!\n    Choose: ");
        //        choice = Console.ReadLine();
        //    }
        //}

        public string ChoiceAsString(string choice)
        {
            string choiceAsString = "";
            if (choice.ToLower() == "create course" || choice.ToLower() == "create teacher" || choice.ToLower() == "add student"
                         || choice.ToLower() == "display information" || choice.ToLower() == "change student grade" || choice.ToLower() == "exit")
            {
                choiceAsString = choice.ToLower();
            }

            return choiceAsString;
        }

        public int ChoiceAsNumber(string choice)
        {
            int choiceAsNumber = 0;
            if (Regex.IsMatch(choice, "^[0-9] +$"))
            {
                choiceAsNumber = int.Parse(choice);
            }

            return choiceAsNumber;
        }

        public string Choose()
        {
            int pick;
            string choice = Console.ReadLine();
            while (Regex.IsMatch(choice, @"[^a-zA-Z0-9\s]"))
            {
                if (Regex.IsMatch(choice, "^[A-Za-z ]+$") || int.TryParse(choice, out pick) == true)
                {
                    if (Regex.IsMatch(choice, "^[A-Za-z ]+$") == true)
                    {
                        if (choice.ToLower() != "create course" || choice.ToLower() != "create teacher" || choice.ToLower() != "add student"
                         || choice.ToLower() != "display information" || choice.ToLower() != "change student grade" || choice.ToLower() != "exit")
                        {
                            Console.Write("\n    (>'-')> Invalid! Please try again!\n    Choose: ");
                            choice = Console.ReadLine();
                        }
                    }
                    else
                    {
                        if (!int.TryParse(choice, out pick) || !(0 < int.Parse(choice) && int.Parse(choice) < 7))
                        {
                            Console.Write("\n    (>'-')> Invalid! Please try again!\n    Choose: ");
                            choice = Console.ReadLine();
                        }
                    }
                }
                else
                {
                    Console.Write("\n    (>'-')> Invalid! Please try again!\n    Choose: ");
                    choice = Console.ReadLine();
                }
            }



            return choice;
        }

        public int NumberOfStudents()
        {
            int number;
            string input = Console.ReadLine();
            while(!int.TryParse(input, out number))
            {
                Console.Write("\n (>'-')> Numbers only! Please try again!\n - ");
                input = Console.ReadLine();
            }

            return number;
        }

        public string LetterAndSpace()
        {
            string input = Console.ReadLine();
            while (!Regex.IsMatch(input, "^[A-Za-z ]+$"))
            {
                Console.Write("\n ( *^*) Invalid, please try again!\r\n - ");
                input = Console.ReadLine();
            }

            return input;

        }

        public decimal Grade()
        {
            decimal grade;
            string point = Console.ReadLine();
            while(!decimal.TryParse(point, out grade) || decimal.Parse(point) < 0 || decimal.Parse(point) > 100)
            {
                Console.Write("\n ( *^*) Invalid, please try again!\n - ");
                point = Console.ReadLine();
            }

            return grade;
        }
        public string SubjectsOfTeacher()
        {
            string subjects = Console.ReadLine();
            while (!Regex.IsMatch(subjects, "^[A-Za-z,]+$"))
            {
                Console.Write("\n ( *^*) Invalid, please try again!\n - ");
                subjects = Console.ReadLine();
            }


            return subjects;
        }

        public int AgeCheck()
        {
            int age;
            string yearsOld = Console.ReadLine();
            while(!int.TryParse(yearsOld, out age) || int.Parse(yearsOld) < 0 || int.Parse(yearsOld) > 100)
            {
                Console.Write("\n (>'-')> No no no, out of range.\n - ");
                yearsOld = Console.ReadLine();
            }

            return age;
        }
    }
}
