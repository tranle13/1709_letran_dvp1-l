﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE03
{
    class Teacher : Person
    {
        private string[] _teacherKnowledge;

    
        public Teacher(string personName, string personDescription, int personAge, string[] teacherKnowledge) : base(personName, personDescription, personAge)
        {
            _teacherKnowledge = new string[teacherKnowledge.Length];
            for (int i = 0; i < teacherKnowledge.Length; i++)
            {
                _teacherKnowledge[i] = teacherKnowledge[i];
            }
        }

        public string[] TeacherKnowledge
        {
            get
            {
                return _teacherKnowledge;
            }
        }
    }
}
