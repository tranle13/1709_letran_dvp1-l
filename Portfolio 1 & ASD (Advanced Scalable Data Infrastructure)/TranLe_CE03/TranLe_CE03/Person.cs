﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE03
{
    class Person
    {
        private string _personName;
        private string _personDescription;
        private int _personAge;

        // Constructor
        public Person(string personName, string personDescription, int personAge)
        {
            _personName = personName;
            _personDescription = personDescription;
            _personAge = personAge;
        }

        public virtual string PersonName
        {
            get
            {
                return _personName;
            }
            set
            {
                _personName = value;
            }
        }

        public virtual string PersonDescription
        {
            get
            {
                return _personDescription;
            }
            set
            {
                _personDescription = value;
            }
        }

         public virtual int PersonAge
        {
            get
            {
                return _personAge;
            }
            set
            {
                _personAge = value;
            }
        }
    }
}
