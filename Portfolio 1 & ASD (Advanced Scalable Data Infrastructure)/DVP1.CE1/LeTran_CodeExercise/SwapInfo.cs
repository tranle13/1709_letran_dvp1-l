﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace LeTran_CodeExercise
{
    class SwapInfo
    {
        public void Input()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("                     *********** SWAP INFO ***********                     ");
            Console.ResetColor();
            ArrayList names = new ArrayList();
            Console.Write("** Hello! What's your first name?\n - First name: ");
            string firstName = Console.ReadLine();
            NameValidation(firstName, names);
            Console.Write("\n** How about your last name?\n Last name: ");
            string lastName = Console.ReadLine();
            NameValidation(lastName, names);
            names.Reverse();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"\n-> Now your name is: {names[0]} {names[1]}.");
            Console.ResetColor();
        }

        public bool NameValidation(string name, ArrayList names)
        {
            while (!Regex.IsMatch(name, "^[A-Za-z]+$"))
            {
                Console.Write("\n( *^*) Noooo, that's not your name!\n - Name: ");
                name = Console.ReadLine();
            }
            names.Add(name);
            return true;
        }
    }
}
