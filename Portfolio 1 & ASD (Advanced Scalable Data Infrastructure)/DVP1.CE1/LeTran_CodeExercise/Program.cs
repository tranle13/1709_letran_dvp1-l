﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeTran_CodeExercise
{
    class Program
    {
        // Name: Tran Le (Sunny)
        // Date: 1709
        // Course: Project & Portfolio 1
        // Synopsis: practice coding with challenges
        static void Main(string[] args)
        {
            // CHALLENGE 5
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("                     *********** M E N U ***********                     ");
                Console.ResetColor();
                Console.WriteLine("\n There are 4 codes in the program. Choose the one that you want to run:");
                Console.WriteLine("     (1). Swap Info.\r\n     (2). Backwards.\r\n     (3). Age Convert.\r\n     (4). Temperature Convert.\r\n     (5). Exit.");
                ChoiceValidate choice = new ChoiceValidate();
                int option = choice.Validation();
                Console.Clear();

                // CHALLENGE 1
                switch (option)
                {
                    case 1:
                        {
                            SwapInfo program1 = new SwapInfo();
                            program1.Input();
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    // CHALLENGE 2
                    case 2:
                        {
                            Backwards program2 = new Backwards();
                            program2.Input();
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    // CHALLENGE 3
                    case 3:
                        {
                            AgeConvert program3 = new AgeConvert();
                            program3.Input();
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    // CHALLENGE 4
                    case 4:
                        {
                            TempConvert program4 = new TempConvert();
                            program4.Input();
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 5:
                    {
                            Environment.Exit(0);
                    }
                        break;
                }
            }
        }
    }
}
