﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeTran_CodeExercise
{
    class TempConvert
    {
        public void Input()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("                     *********** TEMPERATURE CONVERT ***********                     ");
            Console.ResetColor();
            Console.Write("- Which temperature do you like the best?\n  ");
            decimal temp = TempValidation();
            Console.WriteLine("\n- Is it in Fahrenheit or Celcius? (Type \"C\" or \"F\")\n  ");
            string choice = ChoiceValidation();
            TemperatureConvert(temp, choice);
        }
        // Custom functions
        public void TemperatureConvert(decimal temp, string choice)
        {
            if (choice.ToLower() == "f")
            {
                decimal temp1 = (temp - 32) * 5 / 9;
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"--> The temperature is in { temp1.ToString("#,##0.00")} degrees Celcius.");
                Console.ResetColor();
            }
            else
            {
                decimal temp1 = (temp * 9 / 5) + 32;
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"--> The temperature is in {temp1.ToString("#,##.00")} degrees Fahrenheit.");
                Console.ResetColor();
            }
        }

        public decimal TempValidation()
        {
            decimal temper;
            string temp = Console.ReadLine();
            while (!decimal.TryParse(temp, out temper))
            {
                Console.WriteLine("(>'-')> Wrong! Please type number only!");
                temp = Console.ReadLine();
            }
            return temper;
        }

        public string ChoiceValidation()
        {
            string choice = Console.ReadLine();
            while (choice.ToLower() == "c" && choice.ToLower() == "f")
            {
                Console.WriteLine("( :_:) That's not one of the choice, please try again!");
                choice = Console.ReadLine();
            }
            return choice;
        }
    }
}
