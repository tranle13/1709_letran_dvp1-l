﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace LeTran_CodeExercise
{
    class Backwards
    {
        public void Input()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("                     *********** BACKWARDS ***********                     ");
            Console.ResetColor();
            Console.Write("- Type a sentence with at least 6 characters!\n  ");
            string sentence = Console.ReadLine();
            while (sentence.Count() < 6 && Regex.IsMatch(sentence, "^[A-Za-z ]+$"))
            {
                Console.Write("\n- No, your sentence needs to have at least 6 words. Try again!\n  ");
                sentence = Console.ReadLine();
            }
            Console.WriteLine($"\n- Your sentence is: {sentence}.");
            string backwards = new string(sentence.Reverse().ToArray());
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"--> The backward version of your input: {backwards}.");
            Console.ResetColor();
        }
    }
}
