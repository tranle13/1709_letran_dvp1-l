﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeTran_CodeExercise
{
    class AgeConvert
    {
        public void Input()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("                     *********** AGE CONVERT ***********                     ");
            Console.ResetColor();
            Console.Write("- Hey, what's your name?\n  Name: ");
            string name = NameValidation();
            Console.Write("\n- And how old are you?\n  Age:");
            int age = AgeValidation();
            Console.Write("\n- How many leap years have you been through?  Leap year: ");
            int leapYearBeenThrough = LeapYearValidation(age);
            int days = DayConvert(age, leapYearBeenThrough);
            int hours = HourConvert(days);
            int minutes = MinuteConvert(days);
            int seconds = SecondConvert(days);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"--> So your name is {name} and you're {age}. \r\n    It means that you've been living for {days} days, {hours} hours, {minutes} minutes, {seconds} seconds.");
            Console.ResetColor();
        }

        // Custom functions
        public int DayConvert(int age, int leapYearBeenThrough)
        {
            int days = (leapYearBeenThrough * 366) + ((age - leapYearBeenThrough) * 365);
            return days;
        }

        public int HourConvert(int days)
        {
            int hours = days * 24;
            return hours;
        }

        public int MinuteConvert(int days)
        {
            int minutes = days * 24 * 60;
            return minutes;
        }

        public int SecondConvert(int days)
        {
            int seconds = days * 24 * 60 * 60;
            return seconds;
        }

        public string NameValidation()
        {
            string name = Console.ReadLine();
            while (!Regex.IsMatch(name, "^[A-Za-z ]+$"))
            {
                Console.Write("\n   ->  Nuh uh (=-=)! Don't fool me, your name is not like that!\n       ");
                name = Console.ReadLine();
            }


            return name;
        }

        public int AgeValidation()
        {
            int age1;
            string age = Console.ReadLine();
            while (!int.TryParse(age, out age1) || !(0 < int.Parse(age) && int.Parse(age) < 120))
            {
                Console.Write("\n   ->  Hey (>'-')> ! That's not your age!\n       ");
                age = Console.ReadLine();
            }


            return age1;
        }

        public int LeapYearValidation(int age)
        {
            int leapYearBeenThrough;
            string leapYear = Console.ReadLine();
            while (!int.TryParse(leapYear, out leapYearBeenThrough) && !(0 < int.Parse(leapYear) && int.Parse(leapYear) < age))
            {
                Console.WriteLine("\nHaha, don't fool me (*^*), it can only be less than your age.");
                leapYear = Console.ReadLine();
            }

            return leapYearBeenThrough;
        }
    }
}
