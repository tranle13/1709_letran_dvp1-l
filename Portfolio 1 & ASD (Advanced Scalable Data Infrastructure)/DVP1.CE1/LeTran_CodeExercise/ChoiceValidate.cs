﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeTran_CodeExercise
{
    class ChoiceValidate
    {
        public int Validation()
        {
            string option = Console.ReadLine();
            int option1;
            while (!int.TryParse(option, out option1) || !(0 < int.Parse(option) && int.Parse(option) < 6))
            {
                Console.WriteLine("\n-> ( *^*) Only number from 1-5. Please type again!\n   ");
                option = Console.ReadLine();
            }
            return option1;
        }
    }
}
