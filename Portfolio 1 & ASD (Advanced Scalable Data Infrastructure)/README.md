# CODE FOR MY PRACTICAL EXAM #

namespace Le_Tran_PracticalExam
{
    class Program
    {
        /*
        Name: Tran Le (Sunny)
        Date: Jul 28 2017
        Practical exam: the hard one
        */
        static void Main(string[] args)
        {
            // Instatiation and create an ArrayList
            ArrayList employeeNames = new ArrayList();
            Employee store = new Employee();

            Console.WriteLine("     *******************************\r\n     ****** List of Employees ******\r\n     *******************************");

            while (true)
            {
                // Ask for user input and validate the input
                Console.WriteLine("- Choose one the options: < hire >, < fired >, < quit >");
                string choice = Console.ReadLine();
                while (choice != "hire" && choice != "fired" && choice != "quit")
                {
                    Console.WriteLine("Hey! That's not one of the choice, please try again!");
                    choice = Console.ReadLine();
                }

                // Compare the input and show results to console
                if (choice == "hire")
                {
                    employeeNames = store.Hire(choice, employeeNames);
                    ListNames(employeeNames);
                }
                else if (choice == "fired")
                {
                    string num = store.Fired(choice, employeeNames);
                    while (!(0 < int.Parse(num)) || !(int.Parse(num) < employeeNames.Count + 1))
                    {
                        Console.WriteLine("-> Error! Wrong number :P");
                        num = Console.ReadLine();
                    }
                    employeeNames.RemoveAt(int.Parse(num) - 1);
                    ListNames(employeeNames);
                }
                else if (choice == "quit")
                {
                    break;
                }

                // Clear the screen and ask for user's input again
                Console.Clear();
                Console.WriteLine("     *******************************\r\n     ****** List of Employees ******\r\n     *******************************");
                ListNames(employeeNames);
            }
        }

        // Function to list the names
        public static void ListNames(ArrayList e)
        {
            int count = 1;
            foreach (Employee name in e)
            {
                Console.WriteLine(count + ". " + name.mName);
                count++;
            }
        }
    }
