﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE07
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> listOfEmployees = new List<Employee>();
            Employee newEmployee = null;
            Validation validate = new Validation();
            bool running = true;

            while (running)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n" +
                                " |               EMPLOYEE TRACKER MACHINE               |\n" +
                                " <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
                Console.ResetColor();
                Console.Write("\n                 |1|. Add Employee\n" +
                                "                 |2|. Remove Employee\n" +
                                "                 |3|. Display Payroll\n" +
                                "                 |4|. Exit\n" +
                                "              Choose: ");
                Console.ForegroundColor = ConsoleColor.Yellow;
                string choice = Console.ReadLine().ToLower();
                Console.ResetColor();
                Console.Clear();

                switch (choice)
                {
                    case "1":
                    case "add employee":
                    case "add":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n" +
                                            " |               ADD EMPLOYEE               |\n" +
                                            " <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
                            Console.ResetColor();
                            Console.Write("\n                |1|. Fulltime\n" +
                                            "                |2|. Parttime\n" +
                                            "                |3|. Contractor\n" +
                                            "                |4|. Salaried\n" +
                                            "                |5|. Manager\n" +
                                            "             Choose: ");
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            string selection = Console.ReadLine().ToLower();
                            Console.ResetColor();
                            Console.Clear();
                            switch (selection)
                            {
                                case "1":
                                case "fulltime":
                                    {
                                        Console.Write("\n What's the name of the new employee?\n - ");
                                        string ftName = validate.LettersOnly();
                                        Console.Write(" What's his / her address?\n - ");
                                        string ftAddress = Console.ReadLine();
                                        Console.Write(" How much is the payment per hour?\n - $");
                                        decimal ftPayPerHour = validate.DecimalCheck();
                                        newEmployee = new FullTime(ftName, ftAddress, ftPayPerHour);
                                        listOfEmployees.Add(newEmployee);
                                    }
                                    break;
                                case "2":
                                case "parttime":
                                    {
                                        Console.Write("\n What's the name of the new employee?\n - ");
                                        string ptName = validate.LettersOnly();
                                        Console.Write(" What's his / her address?\n - ");
                                        string ptAddress = Console.ReadLine();
                                        Console.Write(" How much is the payment per hour?\n - $");
                                        decimal ptPayPerHour = validate.DecimalCheck();
                                        Console.Write(" How many hours does he / she work per week?\n - ");
                                        decimal ptHourPerWeek = validate.DecimalCheck();
                                        newEmployee = new PartTime(ptName, ptAddress, ptPayPerHour, ptHourPerWeek);
                                        listOfEmployees.Add(newEmployee);
                                    }
                                    break;
                                case "3":
                                case "contractor":
                                    {
                                        Console.Write("\n What's the name of the new employee?\n - ");
                                        string ctName = validate.LettersOnly();
                                        Console.Write(" What's his / her address?\n - ");
                                        string ctAddress = Console.ReadLine();
                                        Console.Write(" How much is the payment per hour?\n - $");
                                        decimal ctPayPerHour = validate.DecimalCheck();
                                        Console.Write(" How many hours does he / she work per week?\n - ");
                                        decimal ctHourPerWeek = validate.DecimalCheck();
                                        Console.Write(" How much is his / her bonus? (in percent)\n - ");
                                        decimal ctNoBenefitsBonus = validate.DecimalCheck(0, 100);
                                        newEmployee = new Contractor(ctName, ctAddress, ctPayPerHour, ctHourPerWeek, ctNoBenefitsBonus);
                                        listOfEmployees.Add(newEmployee);
                                    }
                                    break;
                                case "4":
                                case "salaried":
                                    {
                                        Console.Write("\n What's the name of the new employee?\n - ");
                                        string sl_Name = validate.LettersOnly();
                                        Console.Write(" What's his / her address?\n - ");
                                        string sl_Address = Console.ReadLine();
                                        Console.Write(" How much is his / her yearly salary?\n - $");
                                        decimal sl_Salary = validate.DecimalCheck();
                                        newEmployee = new Salaried(sl_Name, sl_Address, sl_Salary);
                                        listOfEmployees.Add(newEmployee);
                                    }
                                    break;
                                case "5":
                                case "manager":
                                    {
                                        Console.Write(" What's the name of the new employee?\n - ");
                                        string mn_Name = validate.LettersOnly();
                                        Console.Write(" What's his / her address?\n - ");
                                        string mn_Address = Console.ReadLine();
                                        Console.Write(" How much is his / her yearly salary?\n - $");
                                        decimal mn_Salary = validate.DecimalCheck();
                                        Console.Write(" How much is his / her bonus?\n - $");
                                        decimal bonus = validate.DecimalCheck();
                                        newEmployee = new Manager(mn_Name, mn_Address, mn_Salary, bonus);
                                        listOfEmployees.Add(newEmployee);
                                    }
                                    break;
                                default:
                                    {
                                        Console.WriteLine("\n (>'-')> Hmm, this one isn't on the list, please try again!");
                                    }
                                    break;
                            }
                            listOfEmployees.Sort();
                        }
                        break;

                    case "2":
                    case "remove employee":
                    case "remove":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n" +
                                                " |               REMOVE EMPLOYEE               |\n" +
                                                " <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
                            Console.ResetColor();
                            if (listOfEmployees.Count > 0)
                            {
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.WriteLine("            *** List of Employees ***            ");
                                Console.ResetColor();
                                newEmployee.RemoveEmployee(listOfEmployees, validate);
                            }
                            else
                            {
                                Console.WriteLine("\n (>'-')> Empty list! Please add one or more employees to remove!");
                            }
                        }
                        break;

                    case "3":
                    case "display payroll":
                    case "display":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n" +
                                                " |               DISPLAY PAYROLL               |\n" +
                                                " <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
                            Console.ResetColor();
                            if (listOfEmployees.Count > 0)
                            {
                                for (int i = 0; i < listOfEmployees.Count; i++)
                                {
                                    Console.WriteLine($" {i + 1}. {listOfEmployees[i]}\n");
                                }

                            }
                            else
                            {
                                Console.WriteLine("\n (>'-')> Empty list! Please add one or more employees to display payroll!");
                            }
                        }
                        break;

                    case "4":
                    case "exit":
                        {
                            running = false;
                        }
                        break;

                    default:
                        {
                            Console.WriteLine("\n (>'-')> Out of range, please try again!");
                        }
                        break;
                }
                Console.WriteLine("\n Press any key to go back...");
                Console.ReadKey();
            }

        }
    }
}
