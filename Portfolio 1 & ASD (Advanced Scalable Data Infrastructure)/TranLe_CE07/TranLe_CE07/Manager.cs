﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE07
{
    class Manager : Salaried
    {
        private decimal _bonus;

        public Manager(string name, string address, decimal salary, decimal bonus) : base(name, address, salary)
        {
            _salary = salary;
            _bonus = bonus;
        }

        public override decimal CalculatePay()
        {
            decimal totalAmount = _salary + _bonus;
            return totalAmount;
        }

        public override string ToString()
        {
            return $"Name: {Name}\n    Address: {Address}\n    Position: Manager\n    Yearly Revenue: ${CalculatePay()}";
        }

    }
}
