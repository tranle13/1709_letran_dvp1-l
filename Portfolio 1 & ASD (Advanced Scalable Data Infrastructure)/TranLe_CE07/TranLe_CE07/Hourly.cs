﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE07
{
    class Hourly : Employee
    {
        protected decimal _payPerHour;
        protected decimal _hoursPerWeek;

        public Hourly(string name, string address, decimal payPerHour, decimal hoursPerWeek) : base(name, address)
        {
            _payPerHour = payPerHour;
            _hoursPerWeek = hoursPerWeek;
        }

        public override decimal CalculatePay()
        {
            decimal baseAmount = (_payPerHour * _hoursPerWeek) * 4 * 12;

            return baseAmount;
        }
    }
}
