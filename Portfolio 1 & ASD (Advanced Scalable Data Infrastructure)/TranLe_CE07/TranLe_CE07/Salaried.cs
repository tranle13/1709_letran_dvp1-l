﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE07
{
    class Salaried : Employee
    {
        protected decimal _salary;

        public Salaried(string name, string address, decimal salary) : base(name, address)
        {
            _salary = salary;
        }

        public decimal Salary
        {
            get
            {
                return _salary;
            }
        }
        public override string ToString()
        {
            return $"Name: {Name}\n    Address: {Address}\n    Position: Salaried\n    Yearly Revenue: ${_salary}";
        }

    }
}
