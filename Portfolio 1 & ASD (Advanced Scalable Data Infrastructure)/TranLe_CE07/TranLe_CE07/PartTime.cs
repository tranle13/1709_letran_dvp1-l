﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE07
{
    class PartTime : Hourly
    {
        public PartTime(string name, string address, decimal payPerHour, decimal hoursPerWeek) : base(name, address, payPerHour, hoursPerWeek)
        {
            _payPerHour = payPerHour;
            _hoursPerWeek = hoursPerWeek;
        }

        public override decimal CalculatePay()
        {
            return base.CalculatePay();
        }

        public override string ToString()
        {
            return $"Name: {Name}\n    Address: {Address}\n    Position: Parttime\n    Yearly Revenue: ${CalculatePay()}";
        }

    }
}
