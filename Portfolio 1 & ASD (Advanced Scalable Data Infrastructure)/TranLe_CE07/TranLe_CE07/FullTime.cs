﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE07
{
    class FullTime : Hourly
    {
        public FullTime(string name, string address, decimal payPerhour, decimal hoursPerWeek = 40m) : base(name, address, payPerhour, hoursPerWeek)
        {
            _payPerHour = payPerhour;
            _hoursPerWeek = hoursPerWeek;
        }

        public override decimal CalculatePay()
        {
            return base.CalculatePay();
        }

        public override string ToString()
        {
            return $"Name: {Name}\n    Address: {Address}\n    Position: Fulltime\n    Yearly Revenue: ${CalculatePay()}";
        }

    }
}
