﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE07
{
    class Employee : IComparable
    {
        private string _name;
        private string _address;

        public Employee(string name, string address)
        {
            _name = name;
            _address = address;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
            }
        }

        public virtual decimal CalculatePay()
        {
            return 0m;
        }

        public int CompareTo(object newPerson)
        {
            if (newPerson is Employee)
            {
                Employee temp = newPerson as Employee;
                return _name.CompareTo(temp._name);
            }

            return 0;
        }

        public void RemoveEmployee(List<Employee> listOfEmployees, Validation validate)
        {
            for(int i = 0; i < listOfEmployees.Count; i++)
            {
                Console.WriteLine($" {i + 1}. {listOfEmployees[i].Name}");
            }
            Console.Write(" Which unlucky employee do you want to fire?\n - ");
            int fireChoice = validate.ChoiceInRange(1, listOfEmployees.Count);
            listOfEmployees.RemoveAt(fireChoice - 1);
        }

        
    }
}
