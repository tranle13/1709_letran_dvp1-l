﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE07
{
    class Contractor : Hourly
    {
        private decimal _noBenefitsBonus;

        public Contractor(string name, string address, decimal payPerHour, decimal hoursPerWeek, decimal noBenefitsBonus) : base(name, address, payPerHour, hoursPerWeek)
        {
            _noBenefitsBonus = noBenefitsBonus;
        }


        public override decimal CalculatePay()
        {
            decimal totalAmount = base.CalculatePay() + base.CalculatePay() * (_noBenefitsBonus / 100);
            return totalAmount;
        }

        public override string ToString()
        {
            return $"Name: {Name}\n    Address: {Address}\n    Position: Contractor\n    Yearly Revenue: ${CalculatePay()}";
        }

    }
}
