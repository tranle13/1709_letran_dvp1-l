﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TranLe_CE07
{
    class Validation
    {
        public string LettersOnly()
        {
            string input = Console.ReadLine();
            while (!Regex.IsMatch(input, "^[A-Za-z ]+$"))
            {
                Console.Write("\n ( *^*) Invalid, please try again!\r\n - ");
                input = Console.ReadLine();
            }

            return input;

        }

        public decimal DecimalCheck()
        {
            decimal newNum;
            string number = Console.ReadLine();
            while (!decimal.TryParse(number, out newNum))
            {
                Console.Write("\n (>'-')> Man, I ask for the number, not that!\n - ");
                number = Console.ReadLine();
            }


            return newNum;
        }

        public int ChoiceInRange(int min, int max)
        {
            int newNum;
            string number = Console.ReadLine();
            while (!int.TryParse(number, out newNum) || (int.Parse(number) < min || int.Parse(number) > max))
            {
                Console.Write("\n (>'-')> That input isn't on the list so please try again!\n - ");
                number = Console.ReadLine();
            }

            return newNum;
        }

        public decimal DecimalCheck(int min, int max)
        {
            decimal newNum;
            string number = Console.ReadLine();
            while (!decimal.TryParse(number, out newNum) || decimal.Parse(number) < min || decimal.Parse(number) > max)
            {
                Console.Write("\n (>'-')> Wow wow, out of range! Please try again!\n - ");
                number = Console.ReadLine();
            }


            return newNum;
        }

    }
}
