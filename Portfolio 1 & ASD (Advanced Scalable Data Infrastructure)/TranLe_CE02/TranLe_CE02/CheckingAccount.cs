﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE02
{
    class CheckingAccount
    {
        decimal _accountBalance;
        int _accountNumber;

        // Constructor
        public CheckingAccount (decimal accountBalance, int accountNumber)
        {
            _accountBalance = accountBalance;
            _accountNumber = accountNumber;
        }

        // Getters
        public decimal GetAccountBalance()
        {
            return _accountBalance;
        }

        // Setters
        public void SetAccountBalance(decimal accountBalance)
        {
            _accountBalance = accountBalance;
        }
    }
}
