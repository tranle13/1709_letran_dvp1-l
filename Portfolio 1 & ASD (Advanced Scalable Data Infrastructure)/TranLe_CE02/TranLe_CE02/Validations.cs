﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE02
{
    class Validations
    {
        public int NumberValidation()
        {
            int choice;
            string pick = Console.ReadLine();
            while (!int.TryParse(pick, out choice) || !(0 < int.Parse(pick) && int.Parse(pick) < 7))
            {
                Console.WriteLine("\n-> You know that it is wrong man! (*^*)");
                Console.Write("        Choose: ");
                pick = Console.ReadLine();
            }
            return choice;
        }
        public string NameValidation()
        {
            string name = Console.ReadLine();
            while (!Regex.IsMatch(name, "^[A-Za-z]+$"))
            {
                Console.WriteLine("\n-> Nuh uh (>'-')> Your name is not like that!");
                Console.Write(" - Name: ");
                name = Console.ReadLine();
            }


            return name;
        }

        public decimal BalanceValidation()
        {
            decimal balance;
            string money = Console.ReadLine();
            while (!decimal.TryParse(money, out balance))
            {
                Console.WriteLine("\n-> That's not a number D: Try again!");
                Console.Write(" - Account Balance: ");
                money = Console.ReadLine();
            }

            return balance;
        }

        public int AccountNumberValidation()
        {
            int accountNum;
            string accountNumString = Console.ReadLine();
            while (!int.TryParse(accountNumString, out accountNum))
            {
                Console.WriteLine("\n-> That's not a number D: Try again!");
                Console.Write(" - Account Number: ");
                accountNumString = Console.ReadLine();
            }

            return accountNum;
        }
    }
}
