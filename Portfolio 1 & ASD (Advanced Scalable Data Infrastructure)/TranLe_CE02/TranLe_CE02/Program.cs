﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE02
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer currentCustomer = null;
            Validations testing = new Validations();
            while (true)
            {
                Console.WriteLine(" ****** Options:");
                Console.WriteLine("        1. Create customer");
                Console.WriteLine("        2. Create account");
                Console.WriteLine("        3. Set account balance");
                Console.WriteLine("        4. Display account balance");
                Console.WriteLine("        5. Exit");
                Console.Write("        Choose: ");
                int choice = testing.NumberValidation();
                Console.Clear();
                if (choice == 1)
                {
                    if (currentCustomer == null)
                    {
                        Console.WriteLine(" ********** CREATE CUSTOMER ********** ");
                        Console.WriteLine("\n** What is your name?");
                        Console.Write(" - Name: ");
                        string fullName = testing.NameValidation();
                        currentCustomer = new Customer(fullName);
                        Console.WriteLine("\n-> Successfully created new customer (*^O^*)/");
                    }
                }
                else if (choice == 2)
                {
                    if (currentCustomer == null)
                    {
                        Console.WriteLine("\n   (>'-')> You have not created any customer yet.\n   Go back to \"Create customer\" to create new customer.");
                    }
                    else if (currentCustomer != null)
                    {
                        Console.WriteLine(" ********** CREATE ACCOUNT ********** ");
                        Console.WriteLine("\n** Enter information below to create a new account:");
                        Console.Write(" - Account Number: ");
                        int accountsNum = testing.AccountNumberValidation();
                        Console.Write(" - Account Balance: ");
                        decimal accountsBalance = testing.BalanceValidation();
                        currentCustomer.SetCustomerAccount(new CheckingAccount(accountsBalance, accountsNum));
                        Console.WriteLine("\n-> Successfully created new account (*^O^*)/");
                    }
                }
                else if (choice == 3)
                {
                    if (currentCustomer != null && currentCustomer.GetCustomerAccount() != null)
                    {
                        Console.WriteLine(" ********** SET NEW ACCOUNT BALANCE ********** ");
                        Console.Write("\n** Enter your new account balance: ");
                        decimal newAccountBalance = testing.BalanceValidation();
                        currentCustomer.GetCustomerAccount().SetAccountBalance(newAccountBalance);
                        Console.WriteLine($"\n-> Your new account balance is {currentCustomer.GetCustomerAccount().GetAccountBalance()} dollar(s).");
                    }
                    else if (currentCustomer == null)
                    {
                        Console.WriteLine("\n   Haha, don't fool me (*^*) you have not created any customer account yet.\n   Go back to \"Create customer\" and \"Create account\" to create new customer and new account.");
                    }
                    else if (currentCustomer.GetCustomerAccount() == null)
                    {
                        Console.WriteLine("\n   Haha, dont' fool me (*^*) you have not created any account yet.\n   Go back to \"Create account\" to create new customer and new account.");
                    }
                }
                else if (choice == 4)
                {
                    if (currentCustomer != null && currentCustomer.GetCustomerAccount() != null)
                    {
                        Console.WriteLine(" ********** DISPLAY ACCOUNT BALANCE ********** ");
                        Console.WriteLine($"\n-> Your current account balance is {currentCustomer.GetCustomerAccount().GetAccountBalance()} dollar(s).\n   Wow! You're rich, man! (*O*)");
                    }
                    else if (currentCustomer == null || currentCustomer.GetCustomerAccount() == null)
                    {
                        Console.WriteLine($"\n   (>'-')> | You have not create an account yet.\n       | Go back to \"Create Account\" to create a new account.");
                    }
                }
                else
                {
                    break;
                }
                Console.WriteLine("\nPress any key to to go back...");
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
