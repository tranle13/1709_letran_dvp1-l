﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE02
{
    class Customer
    {
        CheckingAccount _customerAccount = null;
        string _customerName;

        // Constructor
        public Customer (string customerName)
        {
            _customerName = customerName;
        }

        // Getters
        public CheckingAccount GetCustomerAccount()
        {
            return _customerAccount;
        }

        // Setters
        public void SetCustomerAccount(CheckingAccount customerAccount)
        {
            _customerAccount = customerAccount;
        }

    }
}
