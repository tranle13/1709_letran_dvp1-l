﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE01
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tank = { "burgundy", "turquoise", "yellow", "lavender", "lime", "peach", "turquoise", "peach", "burgundy","lavender", "yellow", "burgundy", "yellow", "turquoise", "peach", "turquoise" };
            float[] length = { 10.8f, 12f, 13.8f, 8.3f, 2.4f, 1.2f, 3.6f, 7.4f, 6.1f, 10.2f, 11.3f, 4.5f, 23.1f, 15.4f, 16.3f, 9.2f };
            string[] color = { "burgundy", "turquoise", "yellow", "lime", "peach", "lavender" };
            Console.WriteLine("\nPick the color that you like");
            Console.WriteLine("     1. Burgundy");
            Console.WriteLine("     2. Turquoise");
            Console.WriteLine("     3. Yellow");
            Console.WriteLine("     4. Lime");
            Console.WriteLine("     5. Peach");
            Console.WriteLine("     6. Lavender");
            Console.Write("Answer: ");
            int choice = Validation();

            float maxValue = 0f;

            for (int i = 0; i < tank.Length; i++)
            {
                if (color[choice - 1] == tank[i])
                {
                    if (maxValue < length[i])
                    {
                        maxValue = length[i];
                    }
                }
            }
            Console.WriteLine($"-> The longest {color[choice - 1]} fish is {maxValue} centimeters.");
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        static int Validation()
        {
            int choice;
            string pick = Console.ReadLine();
            while (!int.TryParse(pick, out choice) || !(0 < int.Parse(pick) && int.Parse(pick) < 7))
            {
                Console.WriteLine("-> Uh oh! Please try again!");
                pick = Console.ReadLine();
            }
            return choice;
        }

    }
}
