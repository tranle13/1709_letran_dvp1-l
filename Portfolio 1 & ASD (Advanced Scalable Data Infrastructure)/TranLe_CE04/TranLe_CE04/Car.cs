﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE04
{
    class Car
    {
        private string _make;
        private string _model;
        private string _color;
        private float _mileage;
        private int _year;
        
        public Car(string make, string model, string color, float mileage, int year)
        {
            _make = make;
            _model = model;
            _color = color;
            _mileage = mileage;
            _year = year;
        }
        

        public void Drive(float newMileage)
        {
            _mileage += newMileage;
            Program.GetLogger().LogW(_mileage.ToString());
        }
    }
}
