﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE04
{
    class Program
    {
        private static Logger logIn = null;

        public static Logger GetLogger()
        {
            return logIn;
        }
        static void Main(string[] args)
        {
            Car currentCar = null;
            Validation validate = new Validation();
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write(" ********** CAR FACTORY ********** \n");
                Console.ResetColor();
                Console.Write("         1. Disable logging\n" +
                              "         2. Enable logging\n" +
                              "         3. Create a car\n" +
                              "         4. Drive the car\n" +
                              "         5. Destroy the car\n" +
                              "         6. Exit\n" +
                              "    Choose: ");
                string choice = validate.ChoiceString();
                Console.Clear();
                switch (choice.ToLower())
                {
                    case "1":
                    case "disable logging":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(" ******** DISABLE LOGGING ******** ");
                            Console.ResetColor();
                            logIn = new DoNotLog();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("\n Successfully disabled logging! See ya!");
                            Console.ResetColor();
                        }
                        Console.WriteLine("\n Press any key to go back...");
                        Console.ReadKey();
                        Console.Clear();
                        continue;

                    case "2":
                    case "enable logging":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(" ******** ENABLE LOGGING ******** ");
                            Console.ResetColor();
                            logIn = new LogToConsole();
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("\n Successfully enabled logging! Welcome!");
                            Console.ResetColor();
                        }
                        Console.WriteLine("\n Press any key to go back...");
                        Console.ReadKey();
                        Console.Clear();
                        continue;
                    case "3":
                    case "create a car":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(" ******** CREATE A CAR ******** ");
                            Console.ResetColor();
                            if (logIn == null || (logIn is DoNotLog) == true)
                            {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("\n (>'-')> Mm can you go back to \"Enable logging\" to enable logging?\n I can't create new car if you haven't logged in.");
                                Console.ResetColor();
                            }
                            else
                            {
                                Console.Write("\n Which car brand would you choose for your car?\n - ");
                                string make = validate.LettersOnly();
                                logIn.LogD(make);
                                Console.Write("\n Which model would you give your car?\n - ");
                                string model = validate.ModelCheck();
                                logIn.LogD(model);
                                Console.Write("\n And your favorite color is?\n - ");
                                string color = validate.LettersOnly();
                                logIn.LogD(color);
                                Console.Write("\n How much is the mileage gonna be?\n - ");
                                float mileage = validate.mileageCheck();
                                logIn.LogD(mileage.ToString());
                                Console.Write("\n Which year do you think World War 3 will happen?\n - ");
                                int year = validate.yearCheck();
                                logIn.LogD(year.ToString());
                                currentCar = new Car(make, model, color, mileage, year);
                            }
                        }
                        Console.WriteLine("\n Press any key to go back...");
                        Console.ReadKey();
                        Console.Clear();
                        continue;
                    case "4":
                    case "drive the car":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(" ******** DRIVE THE CAR ******** ");
                            Console.ResetColor();
                            if (currentCar == null || (logIn is DoNotLog) == true)
                            {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("\n (^._.^) Hmm I have no cars to display.\n Go back to \"Create a car\" to make one and \"Enable logging\" if you haven't logged in.");
                                Console.ResetColor();
                            }
                            else
                            {
                                Console.Write("\n How far have you been driving in your new car?\n - ");
                                float newMileage = validate.mileageCheck();
                                currentCar.Drive(newMileage);
                            }
                        }
                        Console.WriteLine("\n Press any key to go back...");
                        Console.ReadKey();
                        Console.Clear();
                        continue;
                    case "5":
                    case "destroy the car":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(" ******** DESTROY THE CAR ******** ");
                            Console.ResetColor();
                            if (currentCar == null || (logIn is DoNotLog) == true)
                            {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("\n (^._.^) Mmm hey, you have no cars to destroy.\n Go back to \"Create a car\" to make one and \"Enable logging\" if you haven't logged in.");
                                Console.ResetColor();
                            }
                            else
                            {
                                currentCar = null;
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("\n Succesfully destroy the car! You're a badass!!");
                                Console.ResetColor();
                            }
                        }
                        Console.WriteLine("\n Press any key to go back...");
                        Console.ReadKey();
                        Console.Clear();
                        continue;
                    case "6":
                    case "exit":
                        {
                            Environment.Exit(0);
                        }
                        break;

                }
            }
        }
    }
}
