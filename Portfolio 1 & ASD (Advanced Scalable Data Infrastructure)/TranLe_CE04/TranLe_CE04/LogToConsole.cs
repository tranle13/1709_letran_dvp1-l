﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE04
{
    class LogToConsole : Logger
    {
        public override void Log(string input)
        {
            _lineNumber++;
            Console.WriteLine($"{_lineNumber}. {input}");
        }

        public override void LogD(string input)
        {
            _lineNumber++;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"{_lineNumber}. {input} -- DEBUG!");
            Console.ResetColor();
        }
        
        public override void LogW(string input)
        {
            _lineNumber++;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{_lineNumber}. {input} -- WARNING!");
            Console.ResetColor();
        }
    }
}
