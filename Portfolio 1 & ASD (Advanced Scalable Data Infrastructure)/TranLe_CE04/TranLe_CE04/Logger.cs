﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE04
{
    abstract class Logger : ILog
    {
        protected static int _lineNumber;
        abstract public void Log(string input);
        abstract public void LogD(string input);
        abstract public void LogW(string input);
    }
}
