﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranLe_CE04
{
    interface ILog
    {
        void Log(string input);

        void LogD(string input);

        void LogW(string input);
    }
}
