﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TranLe_CE04
{
    class Validation
    {
        public string ChoiceString()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            string choice = Console.ReadLine();
            Console.ResetColor();
            string[] options = { "disable logging", "enable logging", "create a car", "drive the car", " destroy the car", "exit", "1", "2", "3", "4", "5", "6" };
            while (!options.Contains(choice.ToLower()))
            {
                Console.Write("\n    (>'-')> Invalid! Please try again!\n    Choose: ");
                Console.ForegroundColor = ConsoleColor.Yellow;
                choice = Console.ReadLine();
                Console.ResetColor();
            }

            return choice;
        }


        public string LettersOnly()
        {
            string input = Console.ReadLine();
            while (!Regex.IsMatch(input, "^[A-Za-z ]+$"))
            {
                Console.Write("\n ( *^*) Invalid, please try again!\r\n - ");
                input = Console.ReadLine();
            }

            return input;

        }

        public float mileageCheck()
        {
            float floatMileage;
            string mileage = Console.ReadLine();
            while(!float.TryParse(mileage, out floatMileage))
            {
                Console.Write("\n ( *^*) Invalid, please try again!\n - ");
                mileage = Console.ReadLine();
            }

            return floatMileage;
        }
        public int yearCheck()
        {
            int newYear;
            string year = Console.ReadLine();
            while (!int.TryParse(year, out newYear))
            {
                Console.Write("\n (>'-')> Man, I ask for the year, not that!\n - ");
                year = Console.ReadLine();
            }


            return newYear;
        }

        public string ModelCheck()
        {
            string make = Console.ReadLine();
            while(Regex.IsMatch(make, @"[^\w\s]"))
            {
                Console.Write("\n (>'-')> No no no, wrong wrong wrong.\n - ");
                make = Console.ReadLine();
            }

            return make;
        }
    }
}
