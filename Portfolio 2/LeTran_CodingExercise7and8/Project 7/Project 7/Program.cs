﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> vocabularies = new List<string>();
            string word = "";
            // Ask user's name
            Console.Write("\n Hey, what's your name?\n - ");
            string name = Console.ReadLine();
            // Ask whether user would help or not
            Console.Write($"\n Hello {name}!\n I'm on a mission to complete a story.\n Can you help me? Pleaseeeeeee? (type \"yes\" or \"no\")\n - ");
            string answer = Console.ReadLine().ToLower();
            // Validate the user input
            while (answer != "yes" && answer != "no")
            {
                Console.Write("\n Hmm I don't understand that. Please try again!\n ");
                answer = Console.ReadLine().ToLower();
            }
            // Cases for the answer
            switch (answer)
            {
                case "no":
                    {
                        Console.WriteLine("\n Oh, sad to see you go but we'll see each other again! XD");
                    }
                    break;
                case "yes":
                    {
                        Console.WriteLine("\n Okay! XD Thank you! Now I'm going to ask you a series of word.");
                        Console.Write("\n What would be the best adjective to describe your day?\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.Write("\n What is your favorite animal?\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.Write("\n What do you like to do? (remember to write it in past tense!)\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.Write("\n What would be the best adverb describe deadlines?\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.Write("\n Write an adjective that starts with a \"z\"!\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.Write("\n Write a noun that starts with a \"x\"!\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.Write("\n What object do you wish you can own right now? (pick a noun!)\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.Write("\n Hmm pick an adjective that indicates size!\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.Write("\n Can you choose a fancy verb for me? Type it below please\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.Write("\n Now pick an abverd that goes well with the verb you just chose!\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.Write("\n Type in a verb about talking and type it in past tense!\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.Write("\n And finally, choose an adjective, whatever adjective will work! *wink*\n - ");
                        word = Console.ReadLine();
                        vocabularies.Add(word);
                        Console.WriteLine("\n Whew!!! Thanks for your help! XD Now enjoy the story!!!");
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.WriteLine("\n                                   DAY AT THE ZOO!!!");
                        Console.ResetColor();
                        Console.WriteLine($"\n      Today I went to the zoo. I saw a {vocabularies[0]} {vocabularies[1]} jumping up and down in its tree. " +
                                            $"He {vocabularies[2]} {vocabularies[3]} through the large tunnel that led to its {vocabularies[4]} {vocabularies[5]}. " +
                                            $"I got some peanuts and passed them through the cage to a gigantic gray {vocabularies[6]} towering above my head. Feeding " +
                                            $"that animal made me hungry. I went to get a {vocabularies[7]} scoop of ice cream. It filled my stomach. Afterwards I had " +
                                            $"to {vocabularies[8]} {vocabularies[9]} to catch our bus. When I got home I {vocabularies[10]} my mom for a {vocabularies[11]} " +
                                            $"day at the zoo.");
                    }
                    break;
            }

            Console.Write("\n Press any key to continue");
            Console.ReadKey();
        }
    }
}
