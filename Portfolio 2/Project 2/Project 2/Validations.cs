﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_2
{
    class Validations
    {
        public int NumberAndRangeCheck(int max)
        {
            int number;
            string num = Console.ReadLine();
            while (!int.TryParse(num, out number) || int.Parse(num) < 1 || int.Parse(num) > max)
            {
                Console.Write("\n (>'-')> No no no, out of range.\n - ");
                num = Console.ReadLine();
            }

            return number;
        }

        public string YesOrNo()
        {
            string answer = Console.ReadLine().ToLower();
            while (answer != "no" && answer != "yes")
            {
                Console.Write("\n D: Yes or no only! Please try again!\n - ");
                answer = Console.ReadLine().ToLower();
            }

            return answer;
        }

        public float FloatCheck()
        {
            string floatnum = Console.ReadLine();
            float realFloat;
            while(!float.TryParse(floatnum, out realFloat))
            {
                Console.Write("\n D: Numbers only! Please try again!\n - ");
                floatnum = Console.ReadLine().ToLower();
            }

            return realFloat;
        }
    }
}
