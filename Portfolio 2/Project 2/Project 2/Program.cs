﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MySql.Data.MySqlClient;
using System.Data;

namespace Project_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Validations check = new Validations();
            Database db = new Database();
            Puppy newPup = null;
            List<Puppy> puppies = new List<Puppy>();
            List<Puppy> currentUser = new List<Puppy>();
            bool start = true;
            bool running = true;
            if (db.Connect())
            {
                while (start)
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("         ---------------------------------------------\n" +
                                      "         |               P E T T I E S               |\n" +
                                      "         ---------------------------------------------");
                    Console.ResetColor();
                    Console.Write(    "\n                       1. Log In\n" +
                                        "                       2. Sign Up\n" +
                                        "                  Choose: ");
                    string inOrUp = Console.ReadLine().ToLower();
                    switch (inOrUp)
                    {
                        case "1":
                        case "log in":
                            {
                                running = LogIn(db);
                            }
                            break;
                        case "2":
                        case "sign up":
                            {
                                running = SignUp();
                            }
                            break;
                    }
                    if (running == true)
                    {
                        start = false;
                    }
                    Console.Write("\n Press any key to continue");
                    Console.ReadKey();
                }


                while (running == true)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("         ---------------------------------------------\n" +
                                      "         |               P E T T I E S               |\n" +
                                      "         ---------------------------------------------");
                    Console.ResetColor();
                    Console.Write(    "                  What do you want to do? ^v^\n" +
                                      "                       1. Adoption\n" +
                                      "                       2. Grooming Time Calculator\n" +
                                      "                       3. Training\n" +
                                      "                       4. Baby-sitting\n" +
                                      "                       5. Healthcare\n" +
                                      "                       6. Exit\n" +
                                      "                  Choose: ");
                    string choice = Console.ReadLine().ToLower();
                    Console.Clear();
                    switch (choice)
                    {
                        case "1":
                        case "adoption":
                            {
                                DataTable data = db.QueryDB("SELECT name, breed, gender, ageInYears, weightInLbs, dob, status FROM petId " +
                                                            "JOIN gender ON petId.genderId = gender.genderId " +
                                                            "JOIN petStatus ON petId.statusId = petStatus.statusId WHERE petStatus.statusId = 1 ORDER BY name;");
                                DataRowCollection rows = data.Rows;

                                foreach (DataRow info in rows)
                                {
                                    string name = info["name"].ToString();
                                    string breed = info["breed"].ToString();
                                    string gender = info["gender"].ToString();
                                    int age = Convert.ToInt32(info["ageInYears"].ToString());
                                    int weight = Convert.ToInt32(info["weightInLbs"].ToString());
                                    string dob = info["dob"].ToString();
                                    string status = info["status"].ToString();
                                    newPup = new Puppy(name, breed, gender, age, weight, dob, status);
                                    puppies.Add(newPup);
                                }
                                for (int i = 0; i < puppies.Count; i++)
                                {
                                    Console.ForegroundColor = ConsoleColor.Yellow;
                                    Console.WriteLine($"\n {i + 1}. Name: {puppies[i].Name}");
                                    Console.ResetColor();
                                    Console.WriteLine($"    Breed: {puppies[i].Breed}\n" +
                                                      $"    Gender: {puppies[i].Gender}\n" +
                                                      $"    Age: {puppies[i].Age}\n" +
                                                      $"    Weight: {puppies[i].Weight}\n" +
                                                      $"    Birthday: {puppies[i].Dob}\n");
                                }
                                Console.Write("\n Which puppy would you like to adopt?\n - ");
                                int adopt = check.NumberAndRangeCheck(puppies.Count);
                                currentUser.Add(puppies[adopt - 1]);
                                puppies.RemoveAt(adopt - 1);
                            }
                            break;
                        case "2":
                        case "grooming time calculator":
                            {
                                Console.Write("\n When did you last have your dog groomed? (type in numbers)\n - Month: ");
                                int month = check.NumberAndRangeCheck(12);
                                Console.Write(" - Day: ");
                                int day = check.NumberAndRangeCheck(31);
                                day += 15;
                                if (day > 31)
                                {
                                    day -= 31;
                                    month += 1;
                                }
                                Console.WriteLine($"\n The next day to get your doggo groomed is {month}/{day}/2017");
                                Console.Write("\n Would you like to set reminder?(type in \"yes\" or \"no\")\n - ");
                                string answer = check.YesOrNo();
                                switch (answer)
                                {
                                    case "yes":
                                        {
                                            Console.WriteLine("\n Reminder is successfully set! XD");
                                        }
                                        break;
                                }
                            }
                            break;
                        case "3":
                        case "training":
                            {
                                TrainAndWalk(db, "SELECT CONCAT(firstname, ' ', lastname) AS fullname, rating, age, location, email, phoneNumber FROM coaches_Info;");
                            }
                            break;
                        case "4":
                        case "baby-sitting":
                            {
                                TrainAndWalk(db, "SELECT CONCAT(firstname, ' ', lastname) AS fullname, rating, age, location, email, phoneNumber FROM sitters_Info;");
                            }
                            break;
                        case "5":
                        case "healthcare":
                            {
                                Console.Write("                   What do you want to do? ^v^\n" +
                                              "                          1. Edible food\n" +
                                              "                          2. Vet clinics\n" +
                                              "                          3. First-aid\n" +
                                              "                          4. Vaccines Time Calculator\n" +
                                              "                          5. Weightloss Time Calculator\n" +
                                              "                     Choose: ");
                                string action = Console.ReadLine().ToLower();
                                switch (action)
                                {
                                    case "1":
                                    case "edible food":
                                        {
                                            DataTable data = db.QueryDB("SELECT * FROM edibleFood;");
                                            DataRowCollection rows = data.Rows;
                                            int counter = 1;
                                            foreach (DataRow info in rows)
                                            {
                                                string foodname = info["foodName"].ToString();
                                                string explanation = info["explanation"].ToString();
                                                Console.ForegroundColor = ConsoleColor.Green;
                                                Console.Write($"\n {counter}. {foodname}");
                                                Console.ResetColor();
                                                Console.Write($"\n    {explanation}\n");
                                                counter++;
                                            }
                                        }
                                        break;
                                    case "2":
                                    case "vet clinics":
                                        {
                                            DataTable data = db.QueryDB("SELECT * FROM vetClinics;");
                                            DataRowCollection rows = data.Rows;
                                            int counter = 1;
                                            foreach (DataRow info in rows)
                                            {
                                                string clinicName = info["name"].ToString();
                                                string location = info["location"].ToString();
                                                string phoneNumber = info["phoneNumber"].ToString();
                                                Console.ForegroundColor = ConsoleColor.Magenta;
                                                Console.Write($"\n {counter}. {clinicName}");
                                                Console.ResetColor();
                                                Console.Write($"\n    Location: {location}\n" +
                                                              $"    Phone: {phoneNumber}\n");
                                                counter++;
                                            }
                                        }
                                        break;
                                    case "3":
                                    case "first-aid":
                                        {
                                            DataTable data = db.QueryDB("SELECT * FROM firstAid;");
                                            DataRowCollection rows = data.Rows;
                                            int counter = 1;
                                            foreach (DataRow info in rows)
                                            {
                                                string firstaidName = info["name"].ToString();
                                                string instruction = info["instruction"].ToString();
                                                Console.ForegroundColor = ConsoleColor.Magenta;
                                                Console.Write($"\n {counter}. {firstaidName}:");
                                                Console.ResetColor();
                                                Console.Write($" {instruction}\n");
                                                counter++;
                                            }
                                        }
                                        break;
                                    case "4":
                                    case "vaccines time calculator":
                                        {
                                            Console.Write("\n When did you last have your dog vaccinated? (type in numbers)\n - Month: ");
                                            int month = check.NumberAndRangeCheck(12);
                                            Console.Write(" - Day: ");
                                            int day = check.NumberAndRangeCheck(31);
                                            day += 30;
                                            if (day > 30)
                                            {
                                                day -= 30;
                                                month += 1;
                                            }
                                            Console.WriteLine($"\n The next day to get your pup vaccinated is {month}/{day}/2017");
                                            Console.Write("\n Would you like to set reminder?(type in \"yes\" or \"no\")\n - ");
                                            string answer = check.YesOrNo();
                                            switch (answer)
                                            {
                                                case "yes":
                                                    {
                                                        Console.WriteLine("\n Reminder is successfully set! XD");
                                                    }
                                                    break;
                                            }
                                        }
                                        break;
                                    case "5":
                                    case "weightloss time calculator":
                                        {
                                            Console.Write("\n What is the weight of your pup now?\n - ");
                                            float currentWeight = check.FloatCheck();
                                            Console.Write("\n What is the desired weight that you want?\n - ");
                                            float desiredWeight = check.FloatCheck();
                                            Console.Write("\n How many percent of kcal do you want to feed your pup on diet?\n - ");
                                            float percentDiet = check.NumberAndRangeCheck(100);
                                            float kcal_1 = (desiredWeight * 0.454f) * 132f;
                                            float kcal_2 = kcal_1 * (percentDiet / 100);
                                            float kcal_3 = kcal_1 - kcal_2;
                                            float kcal_4 = 3500f / kcal_3;
                                            double daysToLoseWeight = Math.Floor(kcal_4 * (currentWeight - desiredWeight));
                                            Console.WriteLine($"\n So it will take your pup {daysToLoseWeight} days to reach desired weight! XD");
                                        }
                                        break;

                                }
                            }
                            break;

                    }
                    Console.Write("\n Press any to continue");
                    Console.ReadKey();
                }
            }
        }

        public static void TrainAndWalk(Database db, string query)
        {
            DataTable data = db.QueryDB(query);
            DataRowCollection rows = data.Rows;
            int counter = 1;
            foreach (DataRow info in rows)
            {
                string fullname = info["fullname"].ToString();
                float rating = float.Parse(info["rating"].ToString());
                int age = Convert.ToInt32(info["age"].ToString());
                string location = info["location"].ToString();
                string email = info["email"].ToString();
                string phoneNumber = info["phoneNumber"].ToString();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write($"\n {counter}. {fullname}");
                Console.ResetColor();
                Console.Write($"    Rating: {rating}\n" +
                              $"    Age: {age}\n" +
                              $"    Location: {location}\n" +
                              $"    Email: {email}\n" +
                              $"    Phone: {phoneNumber}\n");
                counter++;
            }
        }

        private static bool LogIn(Database db)
        {
            bool loggedIn = false;
            Console.Write("- Username: ");
            string input1 = Console.ReadLine();
            Console.Write("- Password: ");
            string input2 = Console.ReadLine();
            DataTable data = db.QueryDB($"SELECT username FROM userId WHERE username = {input1};");
            DataRowCollection rows = data.Rows;
            string username = "";
            string pwd = "";
            foreach (DataRow info in rows)
            {
                username = info["username"].ToString();
            }
            while (username != "")
            {
                if (input1 == username && input2 == pwd)
                {
                    Console.WriteLine("\n Successfully logged in!");
                    loggedIn = true;
                }
                else
                {
                    Console.WriteLine("\n Please try again!");
                }
            }
            if (username == "")
            {
                Console.WriteLine("\n Oh that username does not exist. Please go back and create an account.");
            }

            return loggedIn;
        }

        private static bool SignUp()
        {
            Console.WriteLine("\n Fill in these fields to sign up:");
            Console.Write("\n 1. Username: ");
            string username = Console.ReadLine();
            Console.Write("\n 2. Email: ");
            string email = Console.ReadLine();
            Console.Write("\n 3. First name: ");
            string firstName = Console.ReadLine();
            Console.Write("\n 4. Last name: ");
            string lastName = Console.ReadLine();
            Console.Write("\n 5. Password: ");
            string password = Console.ReadLine();

            string signUpCommand = $"INSERT INTO userId(username, email, password, firstName, lastName) VALUES ({username}, {email}, {password}, {firstName}, {lastName});";
            MySqlCommand cmd = new MySqlCommand(signUpCommand);
            return true;
        }


    }
}
