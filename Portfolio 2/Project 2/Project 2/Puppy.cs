﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_2
{
    class Puppy
    {
        string _name;
        string _breed;
        string _gender;
        int _age;
        int _weight;
        string _dob;
        string _status;

        public Puppy(string name, string breed, string gender, int age, int weight, string dob, string status)
        {
            _name = name;
            _breed = breed;
            _gender = gender;
            _age = age;
            _weight = weight;
            _dob = dob;
            _status = status;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Breed
        {
            get { return _breed; }
            set { _breed = value; }
        }

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public int Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public string Dob
        {
            get { return _dob; }
            set { _dob = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
    }
}
