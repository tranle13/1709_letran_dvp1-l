﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_5
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create an array with options
            string[] options = { "rock", "paper", "scissors" };
            List<string> results = new List<string>();
            List<string> userChoice = new List<string>();
            List<string> comChoice = new List<string>();
            // Say hello to user and ask them how many times they want to play rock, paper, and scissors
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("\n Hey hey hey!! Welcome to Rock, Paper, and Scissors.\n You don't have to play alone, you can play with me XD\n How many times do you want to play?\n - ");
            Console.ResetColor();
            string _playTimes = Console.ReadLine();
            // Validation for input
            int playTimes;
            while (!int.TryParse(_playTimes, out playTimes) || int.Parse(_playTimes) < 1)
            {
                Console.Write("\n Mmm I need numbers and it has to be larger than 0. Please try again!\n - ");
                _playTimes = Console.ReadLine();
            }
            Console.Write("\n Press any key to continue.");
            Console.ReadKey();
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("              ========================================\n" +
                          "              |            IT'S GAME TIME            |\n" +
                          "              ========================================");
            Console.ResetColor();
            // Loop to continue asking user
            for (int i = 0; i < playTimes; i++)
            {
                // Ask user about which option they choose
                Console.Write("\n Which one would you choose: Rock, Paper, Scissors? (type in the options)\n - ");
                string choice = Console.ReadLine().ToLower();
                while (choice != "rock" && choice != "paper" && choice != "scissors")
                {
                    Console.Write("\n Oh hello, you've typed something wrong. Please try again!\n - ");
                    choice = Console.ReadLine().ToLower();
                }
                // Generate random index to get the choice and compare with input
                Random rnd = new Random();
                int index = rnd.Next(0, 3);
                // Call method to compare and add results to the lists
                switch (choice)
                {
                    case "rock":
                        {
                            Addition(results, userChoice, comChoice, options, index, choice, "paper", "scissors", "Computer wins!", "Player wins");
                        }
                        break;
                    case "paper":
                        {
                            Addition(results, userChoice, comChoice, options, index, choice, "scissors", "rock", "Computer wins!", "Player wins");
                        }
                        break;
                    case "scissors":
                        {
                            Addition(results, userChoice, comChoice, options, index, choice, "rock", "paper", "Computer wins!", "Player wins");
                        }
                        break;
                }
            }
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("              ==========================================\n" +
                          "              |            GAME TIME RESULT            |\n" +
                          "              ==========================================");
            Console.ResetColor();
            Console.WriteLine("\n On the right would be your choices and on the left would be my choices XD\n");
            // Loop to show the results to the user
            for (int i = 0; i < results.Count; i++)
            {
                Console.WriteLine($" {i + 1}. {userChoice[i]} - {comChoice[i]}: {results[i]}");
            }
            Console.Write("\n Press any key to continue.");
            Console.ReadKey();
        }

        // Method to continue compare the input and the random choice of computer
        public static void Addition(List<string> results, List<string> userChoice, List<string> comChoice, string[] options, int index, string choice, string obj1, string obj2, string result1, string result2)
        {
            if (choice == options[index])
            {
                results.Add("Tie!");
            }
            else if (options[index] == obj1)
            {
                results.Add(result1);
            }
            else if (options[index] == obj2)
            {
                results.Add(result2);
            }
            userChoice.Add(choice);
            comChoice.Add(options[index]);
        }
    }
}
