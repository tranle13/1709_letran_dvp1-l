﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Players player = null;
            // Different values of cards
            string[] cards = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace" };
            int[] pointValue = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 12, 12, 15 };
            string[] suit = { "Hearts", "Diamonds", "Clubs", "Spades" };
            List<string> completeDeck = new List<string>();

            // Array of players' names
            string[] playersName = { "Skip the puppy", "Bob the minion", "Nick the fox", "Rocket the raccoon" };
            List<Players> players = new List<Players>();
            for (int i = 0; i < playersName.Length; i++)
            {
                player = new Players(playersName[i]);
                players.Add(player);
            }

            // Populate complete deck
            for (int i = 0; i < cards.Length; i++)
            {
                for (int j = 0; j < suit.Length; j++)
                {
                    completeDeck.Add($"{cards[i]} {suit[j]}");
                }
            }

            // Loop to create deck for each player
            Random rnd = new Random();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 13; j++)
                {
                    int index = rnd.Next(0, completeDeck.Count);
                    players[i].Deck.Add(completeDeck[index]);
                    completeDeck.RemoveAt(index);
                }
            }
            player.CalculatePoint(players);
            List<Players> sortedList = players.OrderByDescending(a => a.TotalPoint).ToList();
            foreach (Players item in sortedList)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"{item.Name}");
                Console.ResetColor();
                Console.WriteLine($"{item.TotalPoint}");
                for (int i = 0; i < 13; i++)
                {
                    Console.WriteLine(item.Deck[i]);
                }
            }
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }
    }
}
