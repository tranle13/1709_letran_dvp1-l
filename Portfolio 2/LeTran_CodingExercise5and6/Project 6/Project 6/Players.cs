﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_6
{
    class Players
    {
        private string _name;
        private List<string> _deck;
        private int _totalPoint;

        public Players(string name)
        {
            _name = name;
            _deck = new List<string>();
            _totalPoint = 0;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public List<string> Deck
        {
            get
            {
                return _deck;
            }
            set
            {
                _deck = value;
            }
        }

        public int TotalPoint
        {
            get
            {
                return _totalPoint;
            }
            set
            {
                _totalPoint = value;
            }
        }

        public void CalculatePoint(List<Players> players)
        {
            for (int i = 0; i < 4; i++)
            {
                int totalPoint = 0;
                for (int j = 0; j < players[i].Deck.Count; j++)
                {
                    if (players[i].Deck[j].Contains("2"))
                    {
                        totalPoint += 2;
                    }
                    else if (players[i].Deck[j].Contains("3"))
                    {
                        totalPoint += 3;
                    }
                    else if (players[i].Deck[j].Contains("4"))
                    {
                        totalPoint += 4;
                    }
                    else if (players[i].Deck[j].Contains("5"))
                    {
                        totalPoint += 5;
                    }
                    else if (players[i].Deck[j].Contains("6"))
                    {
                        totalPoint += 6;
                    }
                    else if (players[i].Deck[j].Contains("7"))
                    {
                        totalPoint += 7;
                    }
                    else if (players[i].Deck[j].Contains("8"))
                    {
                        totalPoint += 8;
                    }
                    else if (players[i].Deck[j].Contains("9"))
                    {
                        totalPoint += 9;
                    }
                    else if (players[i].Deck[j].Contains("10"))
                    {
                        totalPoint += 10;
                    }
                    else if (players[i].Deck[j].Contains("Jack"))
                    {
                        totalPoint += 12;
                    }
                    else if (players[i].Deck[j].Contains("Queen"))
                    {
                        totalPoint += 12;
                    }
                    else if (players[i].Deck[j].Contains("King"))
                    {
                        totalPoint += 12;
                    }
                    else if (players[i].Deck[j].Contains("Ace"))
                    {
                        totalPoint += 15;
                    }
                }
                players[i].TotalPoint = totalPoint;
            }

        }

    }
}
