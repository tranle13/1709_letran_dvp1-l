﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_4
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create dictionary of colors and theirs theories
            Dictionary<string, string[]> colorInfo = new Dictionary<string, string[]>();
            // Create arrays of colors' theories
            string[] redInfo = { "Red is commonly known for the sign of danger.",
                                 "It represents the passion that associates with love or anger but mostly love.",
                                 "If you look at red for a certain amount of time, you will feel hot." };
            string[] orangeInfo = {"Orange is a round juicy citrus with a tough bright reddish-yellow rind.",
                                   "It is used in Netflix series \"Orange is the new black\".",
                                   "It's one of the worst color to have in the kitchen if you're trying to lose weight"};
            string[] yellowInfo = {"If you mix yellow and green, the combination would turn out to be blue!",
                                   "It gives you the feeling of uplifting and illuminating, offers hope, happiness, cheerfulness, and fun.",
                                   "However, it can be anxiety producing as it's fast moving and can cause us to feel agitated."};
            string[] greenInfo = {"Green contains the powerful energies of nature, growth, desire to expand or increase.",
                                  "Green gemstones are used to attract money, prosperity, and wealth.",
                                  "Green as a noun would mean an environmentalist who belongs to the Green Party."};
            string[] blueInfo = {"Blue represents trust, responsibility, honesty and loyalty.",
                                 "It reduces stress, creates a sense of calmness, relaxation, and order.",
                                 "In case you don't know, blue slows down the metabolism."};
            string[] indigoInfo = {"Indigo is the combination of blue and violet.",
                                   "It can help develop a deeper awareness of what is going on.",
                                   "Indigo stimulates the creative part of the brain and helps you with spatial thinking."};
            string[] violetInfo = {"Violet appears in the visible light spectrum whereas purple is simply a mix of red and blue.",
                                   "Violet assists those who seek the meaning of life and spiritual fulfillment.",
                                   "It relates to the fantasy world, and a need to escape from reality."};
            // Add colors and theories to the Dictionary
            colorInfo.Add("red", redInfo);
            colorInfo.Add("orange", orangeInfo);
            colorInfo.Add("yellow", yellowInfo);
            colorInfo.Add("green", greenInfo);
            colorInfo.Add("blue", blueInfo);
            colorInfo.Add("indigo", indigoInfo);
            colorInfo.Add("violet", violetInfo);
            bool running = true;
            while (running)
            {
                Console.Clear();
                //Write the name of project
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("         ======================================\n" +
                              "         |      C O L O R    L I B R A R Y    |\n" +
                              "         ======================================\n");
                Console.ResetColor();
                // Write out options
                string[] keys = colorInfo.Keys.ToArray();
                for (int i = 0; i < keys.Length; i++)
                {
                    Console.WriteLine($"                    {i+1}. {keys[i]}");
                }
                Console.Write("\n Which color would you want to learn about? (type in the number or color)\n - ");
                Console.ForegroundColor = ConsoleColor.Yellow;
                string choice = Console.ReadLine().ToLower();
                // Switch statement to let user choose, code executes and checks the input
                Console.Clear();
                switch (choice)
                {
                    case "1":
                    case "red":
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write("         =====================================\n" +
                                          "         |               R E D               |\n" +
                                          "         =====================================\n");
                            Console.ResetColor();
                            for (int i = 0; i < colorInfo["red"].Length; i++)
                            {
                                Console.WriteLine($" {i+1}. {colorInfo["red"][i]}");
                            }
                        }
                        break;
                    case "2":
                    case "orange":
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.Write("         ===========================================\n" +
                                          "         |               O R A N G E               |\n" +
                                          "         ===========================================\n");
                            Console.ResetColor();
                            for (int i = 0; i < colorInfo["orange"].Length; i++)
                            {
                                Console.WriteLine($" {i + 1}. {colorInfo["orange"][i]}");
                            }
                        }
                        break;
                    case "3":
                    case "yellow":
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.Write("         ===========================================\n" +
                                          "         |               Y E L L O W               |\n" +
                                          "         ===========================================\n");
                            Console.ResetColor();
                            for (int i = 0; i < colorInfo["yellow"].Length; i++)
                            {
                                Console.WriteLine($" {i + 1}. {colorInfo["yellow"][i]}");
                            }
                        }
                        break;
                    case "4":
                    case "green":
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write("         =========================================\n" +
                                          "         |               G R E E N               |\n" +
                                          "         =========================================\n");
                            Console.ResetColor();
                            for (int i = 0; i < colorInfo["green"].Length; i++)
                            {
                                Console.WriteLine($" {i + 1}. {colorInfo["green"][i]}");
                            }
                        }
                        break;
                    case "5":
                    case "blue":
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write("         =======================================\n" +
                                          "         |               B L U E               |\n" +
                                          "         =======================================\n");
                            Console.ResetColor();
                            for (int i = 0; i < colorInfo["blue"].Length; i++)
                            {
                                Console.WriteLine($" {i + 1}. {colorInfo["blue"][i]}");
                            }
                        }
                        break;
                    case "6":
                    case "indigo":
                        {
                            Console.ForegroundColor = ConsoleColor.DarkBlue;
                            Console.Write("         =====================================\n" +
                                          "         |            I N D I G O            |\n" +
                                          "         =====================================\n");
                            Console.ResetColor();
                            for (int i = 0; i < colorInfo["indigo"].Length; i++)
                            {
                                Console.WriteLine($" {i + 1}. {colorInfo["indigo"][i]}");
                            }
                        }
                        break;
                    case "7":
                    case "violet":
                        {
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            Console.Write("         =====================================\n" +
                                          "         |            V I O L E T            |\n" +
                                          "         =====================================\n");
                            Console.ResetColor();
                            for (int i = 0; i < colorInfo["violet"].Length; i++)
                            {
                                Console.WriteLine($" {i + 1}. {colorInfo["violet"][i]}");
                            }
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("\n Uh oh, out of range!! Please try again! (>'-')>");
                        }
                        continue;
                }
                // Ask user whether they want to continue or not and check the input with switch statement
                Console.Write("\n Do you wanna continue learning about color? (type in \"yes\" or \"no\")\n - ");
                string answer = Console.ReadLine().ToLower();
                while (answer != "yes" && answer != "no")
                {
                    Console.Write("\n I don't know what that is. Please try again!\n - ");
                    answer = Console.ReadLine();
                }
                if (answer == "no")
                {
                    running = false;
                }
            }
            Console.WriteLine("\n Press any key to continue.");
            Console.ReadKey();
        }
    }
}
