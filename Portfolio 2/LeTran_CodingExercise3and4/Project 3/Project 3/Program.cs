﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Project_3
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create an arraylist of first name, last name, and 10 fake grades
            ArrayList info = new ArrayList() { "Sunny", "Le", 89f, 91.21f, 94.6f, 97.77f, 92f, 94.12f, 96.50f, 99.1f, 85.12f, 90.34f };
            // Create a list of 10 fake classes
            string[] classes = { "Pepsi recipe for dummies", "Professional liquor drinker", "What is H2O", "Energy drinks are crazy", "Orange jui-cy",
                                                        "Milk and alcohol", "Nest-tea or nest-tle", "Aquafina ad study", "Corona popularity", "Free arizona tea" };
            Console.WriteLine($"\n Oh hello {info[0]} {info[1]}!!!!!!!!!!\n" +
                              $" I'm glad to see you here!\n" +
                              $" You've been working hard for ten months and now is the time to see the results!\n");
            float totalGrade = 0f;
            for (int i = 0; i < classes.Length; i++)
            {
                string letterGrade = "";
                if (89.5 <= (float)info[i+2] && (float)info[i+2] <= 100)
                {
                    letterGrade = "A";
                }
                else if (79.5 <= (float)info[i + 2] && (float)info[i + 2] <= 89.4)
                {
                    letterGrade = "B";
                }
                else if (72.5 <= (float)info[i + 2] && (float)info[i + 2] <= 79.4)
                {
                    letterGrade = "C";
                }
                else if (69.5 <= (float)info[i + 2] && (float)info[i + 2] <= 72.4)
                {
                    letterGrade = "D";
                }
                else if (0 <= (float)info[i + 2] && (float)info[i + 2] <= 69.4)
                {
                    letterGrade = "F";
                }
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"{i + 1}. {classes[i]}");
                Console.ResetColor();
                Console.WriteLine($"   Grade: {info[i+2]} - {letterGrade}");
                totalGrade += (float)info[i + 2];
            }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"\n -> Your GPA: {totalGrade / 10}");
            Console.ResetColor();
            Console.ReadKey();
        }
    }
}
