﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create banner
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("         =================================================\n" +
                              "         |             TIME FOR QUESTION!!!!             |\n" +
                              "         =================================================");
            Console.ResetColor();
            // Show the question
            Console.Write("\n Which brand of coke do you think that people like the most?\n Please capitalize the first letter and it must only have one word! :)\n ");
            string answer = Console.ReadLine();
            bool running = true;
            // Create loop to continue asking user for the right answer
            while (running)
            {
                if (!char.IsUpper(answer[0]))
                {
                    Console.Write("\n Haha! :D You need to capitalize the first letter!\n ");
                    answer = Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("\n Wow!! You're good! XD");
                    running = false;
                }
            }
            Console.WriteLine("\n Press any key to continue.");
            Console.ReadKey();
        }
    }
}
