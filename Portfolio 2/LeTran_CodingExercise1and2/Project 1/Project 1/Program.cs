﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Project_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create a List of 20 topic matters
            List<string> topicMatters = new List<string>() { "Arizona", "Pepsi", "Coca Cola", "Tropicana", "Nestle", "Mountain Dew", "Monster", "Aquafina", "Dasani", "Nestea", "Gatorade", "Fanta", "Niagara",
                                                             "Heineken", "Corona", "Baileys", "Cacique", "Jack Daniel", "Jim Beam", "Hennessy" };
            // Ask user's name
            Console.Write(" (^o^)/ Boom! What's your name?\n (Don't worry, your information is safe with me *wink*)\n - Name: ");
            string name = Console.ReadLine();
            while (!Regex.IsMatch(name, "^[A-Za-z ]+$"))
            {
                Console.Write("\n (>'-')> I know that's not your name! Letters and spaces only please!\r\n - Name: ");
                name = Console.ReadLine();
            }


            // Show the original list to user
            Console.WriteLine($"\n Oh hey {name}! Now I will show you the original list of bevarage manufacturers I have! :)");
            for (int i = 0; i < topicMatters.Count; i++)
            {
                Console.WriteLine($" {i + 1}. {topicMatters[i]}");
            }
            Console.Write("\n Press any key to continue");
            Console.ReadKey();
            Console.Clear();
            // Show the menu and what the user can do with it
            bool running = true;
            while (running)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("         =================================================\n" +
                                  "         |         WELCOME TO MANUFACTURER WORLD         |\n" +
                                  "         =================================================");
                Console.ResetColor();
                Console.Write("            1. Alphabetize list\n" +
                              "            2. Alphabetize backwards\n" +
                              "            3. Randomly disappear manufacturer until none\n" +
                              "            4. Exit\n" +
                              "       Choose: ");
                Console.ForegroundColor = ConsoleColor.Yellow;
                string choice = Console.ReadLine().ToLower();
                Console.ResetColor();
                Console.Clear();
                switch (choice)
                {
                    case "1":
                    case "alphabetize list":
                        {
                            // Use if-else statement to make sure the list of beverage manufacturers is not empty
                            if (topicMatters.Count != 0)
                            {
                                topicMatters.Sort();
                                for (int i = 0; i < topicMatters.Count; i++)
                                {
                                    Console.WriteLine($"{i + 1}. {topicMatters[i]}");
                                }
                            }
                            else
                            {
                                Console.WriteLine("\n You deleted all of the manufacturers! X-X Please proceed to exit.");
                            }
                        }
                        break;

                    case "2":
                    case "alphabetize backwards":
                        {
                            // Use if-else statement to make sure the list of beverage manufacturers is not empty
                            if (topicMatters.Count != 0)
                            {
                                topicMatters.Sort();
                                topicMatters.Reverse();
                                for (int i = 0; i < topicMatters.Count; i++)
                                {
                                    Console.WriteLine($"{i + 1}. {topicMatters[i]}");
                                }
                                
                            }
                            else
                            {
                                Console.WriteLine("\n You deleted all of the manufacturers! X-X Please proceed to exit.");
                            }
                        }
                        break;

                    // Randomly choose a name from the list and then delete it
                    case "3":
                        {
                            if (topicMatters.Count != 0)
                            {
                                Random rnd = new Random();
                                // Create the loop to continue writing to console what is in the list and removing one random manufacturer at the same time
                                while (topicMatters.Count != 0)
                                {
                                    int index = rnd.Next(0, topicMatters.Count);
                                    for (int i = 0; i < topicMatters.Count; i++)
                                    {
                                        Console.Write($"{i + 1}. {topicMatters[i]} ");
                                    }
                                    Console.WriteLine("\n");
                                    topicMatters.RemoveAt(index);
                                }
                                if (topicMatters.Count == 0)
                                {
                                    Console.WriteLine("Successfully deleted all beverage manufacturers!!!");
                                }
                            }
                            else
                            {
                                Console.WriteLine("\n You deleted all of the manufacturers! X-X Please proceed to exit.");
                            }
                        }
                            break;
                    case "4":
                    case "exit":
                        {
                            running = false;
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("\n Your input is out of range! XoX");
                        }
                        break;
                }
                Console.Write("\n Press any key to continue");
                Console.ReadKey();
            }
        }

    }
}
